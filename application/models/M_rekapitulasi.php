<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_rekapitulasi extends CI_Model {

  public function __construct(){
    parent::__construct();
  }
  
  public function get_all_anggota_by_jenis_anggota(){
    return [
      'Anggota Utama' => $this->get_anggota_by_jenis_anggota('Anggota Utama'), 
      'Anggota Madya' => $this->get_anggota_by_jenis_anggota('Anggota Madya'), 
      'Anggota Pratama' => $this->get_anggota_by_jenis_anggota('Anggota Pratama'), 
      'Anggota Kehormatan' => $this->get_anggota_by_jenis_anggota('Anggota Kehormatan')
    ];
  }

  protected function get_anggota_by_jenis_anggota($jenis_anggota) {
    $this->db->select('*')->where('jenis_anggota', $jenis_anggota);
    $this->db->where('deleted_at', NULL);
    return $this->db->get('tb_anggota')->num_rows();
  }

  public function get_all_anggota_by_jenis_kelamin(){
    return [
      'Laki-Laki' => $this->get_anggota_by_jenis_kelamin('L'),
      'Perempuan' => $this->get_anggota_by_jenis_kelamin('P'),
    ];
  }

  protected function get_anggota_by_jenis_kelamin($jk) {
    $this->db->select('*')->where('jenis_kelamin', $jk);
    $this->db->where('deleted_at', NULL);
    return $this->db->get('tb_anggota')->num_rows();
  }

  public function get_anggota_by_cabang(){
    $this->db->select('tk.keterangan, Count(ta.id) AS jumlah');
    $this->db->join('tb_anggota AS ta', 'ta.cabang = tk.id');
    $this->db->where('ta.deleted_at', NULL);
    $this->db->group_by('tk.keterangan');
    return $this->db->get('tb_kota AS tk')->result_array();
  }

  public function get_all_anggota_by_range_usia(){
    return [
      '18-30' => $this->get_anggota_by_range_usia(18, 30),
      '31-40' => $this->get_anggota_by_range_usia(31, 40),
      '41-50' => $this->get_anggota_by_range_usia(41, 50),
      '51-60' => $this->get_anggota_by_range_usia(51, 60),
      '61-70' => $this->get_anggota_by_range_usia(61, 70),
      '> 70' => $this->get_anggota_by_range_usia(71),
    ];
  }

  protected function get_anggota_by_range_usia($min_usia, $max_usia = 0) {
    $this->db->select('*')->where('deleted_at', NULL);
    $this->db->group_start();
    $this->db->where('(YEAR(NOW()) - YEAR(tanggal_lahir) - (DATE_FORMAT(NOW(), "%m%d") < DATE_FORMAT(tanggal_lahir, "%m%d"))) >=', $min_usia);
    if ($max_usia > 0) $this->db->where('(YEAR(NOW()) - YEAR(tanggal_lahir) - (DATE_FORMAT(NOW(), "%m%d") < DATE_FORMAT(tanggal_lahir, "%m%d"))) <=', $max_usia);
    $this->db->group_end();
    return $this->db->get('tb_anggota')->num_rows();
  }
}

/* End of file M_rekapitulasi.php */
?>