<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_auth extends CI_Model {

  public function __construct(){
    parent::__construct();
  }
  
  public function login($username, $password) {
    $check = $this->db->where('username', $username)->get('cms_users');
    if ($check->num_rows() > 0) {
      $data = $check->row_array();
      if (password_verify($password, $data['password'])) {
        $this->session->set_userdata($data);
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  public function get_current_user(){
    return $this->db->get('cms_users')->row_array();
  }

  public function update_profile($data) {
    return $this->db->update('cms_users', $data);
  }
}

/* End of file M_auth.php */
?>