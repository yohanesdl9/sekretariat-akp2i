<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_anggota extends CI_Model {

  public function __construct(){
    parent::__construct();
  }
  
  public function get_anggota($id = '') {
    if ($id != '') $this->db->where('id', $id);
    $this->db->where('deleted_at', NULL);
    return $this->db->get('tb_anggota');
  }

  public function get_anggota_by_status($status = '') {
    if ($status != '') $this->db->where('status', $status);
    $this->db->where('deleted_at', NULL);
    return $this->db->get('tb_anggota');
  }

  public function get_detail_anggota($id) {
    $this->db->select('ta.*, tl.keterangan AS tempat_lahir, cab.keterangan AS cabang');
    $this->db->join('tb_kota AS cab', 'ta.cabang = cab.id', 'left');
    $this->db->join('tb_kota AS tl', 'ta.tempat_lahir = tl.id', 'left');
    $this->db->where('ta.id', $id);
    return $this->db->get('tb_anggota AS ta')->row_array();
  }

  public function get_realisasi_ppl_anggota($id_anggota, $id = '') {
    if ($id != '') $this->db->where('id', $id);
    $this->db->where('id_anggota', $id_anggota)->where('deleted_at', NULL);
    return $this->db->get('tb_anggota_realisasi_ppl');
  }

  public function get_realisasi_ppl_anggota_tahun($id_anggota, $tahun = 'all') {
    if ($tahun != 'all') $this->db->where('tahun_laporan', $tahun);
    $this->db->where('id_anggota', $id_anggota)->where('deleted_at', NULL);
    return $this->db->get('tb_anggota_realisasi_ppl');
  }

  public function insert_anggota($data) {
    return $this->db->insert('tb_anggota', $data);
  }

  public function update_anggota($id, $data) {
    return $this->db->update('tb_anggota', $data, ['id' => $id]);
  }

  public function delete_anggota($id) {
    $data = ['deleted_at' => date('Y-m-d H:i:s')];
    return $this->update_anggota($id, $data);
  }

  public function insert_realisasi_ppl($data) {
    return $this->db->insert('tb_anggota_realisasi_ppl', $data);
  }

  public function update_realisasi_ppl($id, $data) {
    return $this->db->update('tb_anggota_realisasi_ppl', $data, ['id' => $id]);
  }

  public function delete_realisasi_ppl($id) {
    $data = ['deleted_at' => date('Y-m-d H:i:s')];
    return $this->update_realisasi_ppl($id, $data);
  }

}

/* End of file ModelName.php */
?>