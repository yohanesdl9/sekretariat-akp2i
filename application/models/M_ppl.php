<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ppl extends CI_Model {
  
  public function __construct(){
    parent::__construct();
  }
  
  public function get_ppl($id_ppl = '') {
    if ($id_ppl != '') $this->db->where('id', $id_ppl);
    $this->db->where('deleted_at', NULL);
    return $this->db->get('tb_ppl');
  }

  public function insert_ppl($data) {
    return $this->db->insert('tb_ppl', $data);
  }

  public function update_ppl($id, $data) {
    return $this->db->update('tb_ppl', $data, ['id' => $id]);
  }

  public function delete_ppl($id) {
    $data = ['deleted_at' => date('Y-m-d H:i:s')];
    return $this->update_ppl($id, $data);
  }

}

/* End of file M_ppl.php */
?>