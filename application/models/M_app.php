<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class M_app extends CI_Model {

  public function __construct(){
    parent::__construct();
  }

  function listProvinsi(){
    $this->db->select('*');
    $this->db->from('tb_provinsi');
    $sql = $this->db->get();
    return $sql->result_array();
  }

  function listKota($id_provinsi){
    $this->db->where('id_provinsi', $id_provinsi);
    return $this->db->get('tb_kota')->result_array();
  }

  function dropdown_area(){
    $data = $this->db->where('deleted_at', NULL)->get('tb_provinsi')->result();
    foreach ($data as $d) {
      $d->kota = $this->listKota($d->id);
    }
    return $data;
  }
  
  function getLatestId($key, $table) {
    $this->db->select_max($key);
    $query = $this->db->get($table);
    $result = $query->row_array();
    return $result[$key] + 1;
  }

  function getDataByParameter($parameters, $values, $tables){
    return $this->db->where($parameters, $values)->get($tables);
  }

  function getDataByParameters($parameters, $tables, $order = NULL, $order_mode = "ASC"){
    $this->db->where($parameters);
    if($order != NULL) $this->db->order_by($order, $order_mode);
    return $this->db->get($tables);
  }

  function setAlert($alert_type, $message){
    $this->session->set_flashdata('color', $alert_type);
    $this->session->set_flashdata('message', $message);
  }

  function setAlertIfSuccessOrFailed($process, $success_message, $failed_message) {
    if ($process == TRUE) {
      $this->setAlert('success', $success_message);
    } else {
      $this->setAlert('danger', $failed_message);
    }
  }
}
?>