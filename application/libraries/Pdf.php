<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

use Dompdf\Dompdf;
use Dompdf\Options;

class Pdf extends Dompdf {
  
  public function __construct(){
    parent::__construct();
  }

  protected function ci(){
    return get_instance();
  }

  public function load_view($view, $filename, $data = array()){
    $options = new Options();
    $options->setDefaultFont('Helvetica');

    $html = $this->ci()->load->view($view, $data, TRUE);
    
    $this->setOptions($options);
    $this->load_html($html);
    $this->render();
    $this->stream($filename, array("Attachment" => false));
  }
}

/* End of file Pdf extends Dompdf.php */
 ?>