<div class="row">
  <div class="col-md-12">
    <div class="mb-2">
      <a href="<?= base_url('anggota') ?>"><i class="fas fa-chevron-circle-left"></i>Kembali ke halaman Anggota</a>
    </div>
  </div>
  <div class="col-md-4">
    <div class="card">
      <div class="card-body">
        <img src="<?= base_url($anggota['pas_foto']) ?>" width="200" alt="<?= $anggota['nama'] ?>" class="ml-auto mr-auto d-block">
        <h2 class="text-center mt-3"><?= $anggota['nama'] ?></h2>
        <h5 class="text-center"><?= $anggota['no_anggota'] ?></h5>
        <div class="table-responsive-md">
          <table class="table table-borderless">
            <tbody>
              <tr>
                <th>NIK</th>
                <td><?= $anggota['nik'] ?></td>
              </tr>
              <tr>
                <th>No. Izin Konsultan Pajak</th>
                <td><?= $anggota['no_izin_konsultan'] ?></td>
              </tr>
              <tr>
                <th>NPWP</th>
                <td><?= $anggota['npwp'] ?></td>
              </tr>
              <tr>
                <th>Tempat, Tanggal Lahir</th>
                <td><?= (isset($anggota['tempat_lahir']) && $anggota['tanggal_lahir'] != '0000-00-00') ? $anggota['tempat_lahir'] . ', ' . dateIndo($anggota['tanggal_lahir']) : '' ?></td>
              </tr>
              <tr>
                <th>Pekerjaan</th>
                <td><?= $anggota['pekerjaan'] ?></td>
              </tr>
              <tr>
                <th>Nomor HP/Telepon</th>
                <td><?= $anggota['no_hp'] . '/' . $anggota['no_telp'] ?></td>
              </tr>
              <tr>
                <th>Email</th>
                <td><?= $anggota['email'] ?></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-8">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Identitas Anggota</h4>
      </div>
      <div class="card-body">
        <div class="table-responsive-md">
          <table class="table table-borderless">
            <tbody>
              <tr>
                <th>Jenis Anggota (Tingkat)</th>
                <td><?= $anggota['jenis_anggota'] . ' (' . $anggota['tingkat'] . ')' ?></td>
              </tr>
              <tr>
                <th>Tanggal Penetapan</th>
                <td><?= $anggota['tanggal_penetapan'] != '0000-00-00' ? dateIndo($anggota['tanggal_penetapan']) : '' ?></td>
              </tr>
              <tr>
                <th>Cabang</th>
                <td><?= $anggota['cabang'] ?></td>
              </tr>
              <tr>
                <th>Jenis Kelamin</th>
                <td><?= $anggota['jenis_kelamin'] == 'L' ? 'Laki-Laki' : 'Perempuan' ?></td>
              </tr>
              <tr>
                <th>Alamat Rumah</th>
                <td><?= $anggota['alamat_rumah'] . ($anggota['alamat_korespondensi'] == 'Alamat Rumah' ? ' (Alamat Korespondensi)' : '') ?></td>
              </tr>
              <tr>
                <th>Alamat Domisili</th>
                <td><?= $anggota['alamat_domisili'] . ($anggota['alamat_korespondensi'] == 'Alamat Domisili' ? ' (Alamat Korespondensi)' : '') ?></td>
              </tr>
              <tr>
                <th>Alamat Kantor</th>
                <td><?= $anggota['alamat_kantor'] . ($anggota['alamat_korespondensi'] == 'Alamat Kantor' ? ' (Alamat Korespondensi)' : '') ?></td>
              </tr>
              <tr>
                <th>Rekomendasi</th>
                <td><?= $anggota['rekomendasi'] ?></td>
              </tr>
              <tr>
                <th>Pendidikan Terakhir</th>
                <td><?= $anggota['pendidikan_terakhir'] ?></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Data Realisasi PPL</h4>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-3">
            <a href="<?= base_url('anggota/tambah_realisasi_ppl/' . $this->uri->segment(3)) ?>" class="btn btn-success"><i class="fas fa-plus-circle"></i> Tambah Realisasi PPL</a>
          </div>
          <div class="col-md-2 offset-md-6">
            <div class="form-group row ml-auto">
              <?= form_label('Tahun', '', ['class' => 'col-md-4 col-form-label text-right']) ?>
              <div class="col-md-8">
                <select name="tahun" id="filter-tahun" class="form-control">
                  <option value="all">Semua Tahun</option>
                  <?php for ($i = date('Y'); $i >= date('Y', strtotime('-10 years')); $i--) { ?>
                  <option value="<?= $i ?>"><?= $i ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-md-1">
            <a href="<?= base_url('anggota/cetak_ppl/' . $this->uri->segment(3) . '/all') ?>" class="btn btn-success" id="print"><i class="fas fa-print"></i> Cetak PDF</a>
          </div>
        </div>
        <div class="table-responsive-md">
          <table class="table table-bordered table-striped table-hover">
            <thead>
              <tr>
                <th class="text-center">No.</th>
                <th class="text-center">Tahun Laporan</th>
                <th class="text-center">Nama Kegiatan</th>
                <th class="text-center">Tanggal</th>
                <th class="text-center">Tempat</th>
                <th class="text-center">Penyelenggara</th>
                <th class="text-center">Jumlah SKPPL</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody id="ppl_body">
              <tr>
                <td colspan="8">Kegiatan PPL Terstruktur</td>
              </tr>
              <?php $i = 1; foreach ($realisasi_ppl['Terstruktur'] as $rp) { ?>
              <tr>
                <td class="text-center"><?= $i ?></td>
                <td class="text-center"><?= $rp['tahun_laporan'] ?></td>
                <td><?= $rp['nama_kegiatan'] ?></td>
                <td><?= dateIndo($rp['tanggal']) ?></td>
                <td><?= $rp['tempat'] ?></td>
                <td><?= $rp['penyelenggara'] ?></td>
                <td class="text-center"><?= $rp['jumlah_skppl'] ?></td>
                <td class="text-center">
                  <a href="<?= base_url('anggota/edit_realisasi_ppl/' . $this->uri->segment(3) . '/' . $rp['id']) ?>" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-pencil-alt"></i></a>
                  <a href="#" class="btn btn-danger" onclick="hapusData('<?= $rp['id'] ?>')" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fas fa-trash-alt"></i></a>
                </td>
              </tr>
              <?php $i++; } ?>
              <tr>
                <td colspan="8">Kegiatan PPL Tidak Terstruktur</td>
              </tr>
              <?php $i = 1; foreach ($realisasi_ppl['Tidak Terstruktur'] as $rp) { ?>
              <tr>
                <td class="text-center"><?= $i ?></td>
                <td class="text-center"><?= $rp['tahun_laporan'] ?></td>
                <td><?= $rp['nama_kegiatan'] ?></td>
                <td><?= dateIndo($rp['tanggal']) ?></td>
                <td><?= $rp['tempat'] ?></td>
                <td><?= $rp['penyelenggara'] ?></td>
                <td class="text-center"><?= $rp['jumlah_skppl'] ?></td>
                <td class="text-center">
                  <a href="<?= base_url('anggota/edit_realisasi_ppl/' . $this->uri->segment(3) . '/' . $rp['id']) ?>" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-pencil-alt"></i></a>
                  <a href="#" class="btn btn-danger" onclick="hapusData('<?= $rp['id'] ?>')" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fas fa-trash-alt"></i></a>
                </td>
              </tr>
              <?php $i++; } ?>
              <tr>
                <td class="text-center" colspan="6">Total SKPPL</td>
                <td class="text-center" colspan="2"><?= $jumlah_skppl ?></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$('select[name="tahun"]').change(function(){
  $('#print').attr('href', '<?= base_url('anggota/cetak_ppl/' . $this->uri->segment(3) . '/') ?>' + $(this).val());
  $.ajax({
    url: '<?= base_url('anggota/get_data_realisasi_ppl/' . $this->uri->segment(3) . '/') ?>' + $(this).val(),
    type: 'GET',
    success: function(data) {
      $('#ppl_body').html(data);
    }
  });
});

function hapusData(id){
  Swal.fire({
    title: "Apakah Anda yakin?",
    text: "Anda tidak akan dapat mengembalikan data Anda!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonText: "Ya",
    cancelButtonText: "Tidak"
  }).then((result) => {
    if (result.value) {
      window.location.href = '<?= base_url('anggota/hapus_data_realisasi_ppl/' . $this->uri->segment(3) . '/') ?>' + id;
    }
  });
}
</script>