<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <a href="<?= base_url('anggota/tambah') ?>" class="btn btn-success"><i class="fas fa-plus-circle"></i> Tambah Data</a>
        <div class="table-responsive-md mt-3">
          <table class="table table-bordered table-striped table-hover datatable">
            <thead>
              <tr>
                <th class="text-center">No.</th>
                <th class="text-center">Nama</th>
                <th class="text-center">No. Anggota</th>
                <th class="text-center">Tanggal Penetapan</th>
                <th class="text-center">No. Izin Konsultan Pajak</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
            <?php $i = 1; foreach ($anggota as $p) { ?>
              <tr>
                <td class="text-center"><?= $i ?></td>
                <td><?= $p['nama'] ?></td>
                <td><?= $p['no_anggota'] ?></td>
                <td><?= dateIndo($p['tanggal_penetapan']) ?></td>
                <td><?= $p['no_izin_konsultan'] ?></td>
                <td class="text-center">
                  <a href="<?= base_url('anggota/detail/' . $p['id']) ?>" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fas fa-eye"></i></a>
                  <a href="<?= base_url('anggota/edit/' . $p['id']) ?>" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-pencil-alt"></i></a>
                  <a href="#" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Hapus" onclick="hapusAnggota('<?= $p['id'] ?>')"><i class="fas fa-trash-alt"></i></a>
                </td>
              </tr>
            <?php $i++; } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
function hapusAnggota(id){
  Swal.fire({
    title: "Apakah Anda yakin?",
    text: "Anda tidak akan dapat mengembalikan data Anda!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonText: "Ya",
    cancelButtonText: "Tidak"
  }).then((result) => {
    if (result.value) {
      window.location.href = '<?= base_url('anggota/hapus_anggota/') ?>' + id;
    }
  });
}
</script>