<div class="row">
  <div class="col-md-12">
    <?php if($this->session->flashdata('message')) { ?>
    <div class="alert alert-<?= $this->session->flashdata('color') ?> alert-dismissible mb-3" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <?= $this->session->flashdata('message') ?>
    </div> 
    <?php } ?>
    <div class="mb-2">
      <a href="<?= base_url('anggota') ?>"><i class="fas fa-chevron-circle-left"></i>Kembali ke halaman Anggota</a>
    </div>
    <div class="card">
      <div class="card-body">
        <form action="<?= base_url('anggota/edit_anggota/' . $this->uri->segment(3)) ?>" method="post" enctype="multipart/form-data" id="form">
          <div class="form-group row">
            <?= form_label('Nama Anggota', 'nama', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10"><?= form_input('nama', $anggota['nama'], ['class' => 'form-control']); ?></div>
          </div>
          <div class="form-group row">
            <?= form_label('Jenis Anggota', 'jenis_anggota', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10"><?= form_dropdown('jenis_anggota', $jenis_anggota, $anggota['jenis_anggota'], ['class' => 'form-control']); ?></div>
          </div>
          <div class="form-group row">
            <?= form_label('No. Anggota', 'no_anggota', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10"><?= form_input('no_anggota', $anggota['no_anggota'], ['class' => 'form-control']); ?></div>
          </div>
          <div class="form-group row">
            <?= form_label('Tanggal Penetapan', 'tanggal_penetapan', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10">
              <input type="date" name="tanggal_penetapan" class="form-control" value="<?= $anggota['tanggal_penetapan'] ?>">  
            </div>
          </div>
          <div class="form-group row">
            <?= form_label('Cabang', 'cabang', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10">
              <select name="cabang" class="form-control select2">
                <option disabled selected>Pilih Cabang</option>
                <?php foreach ($area as $a) { ?>
                <optgroup label="<?= $a->keterangan ?>">
                <?php foreach ($a->kota as $k) { ?>
                  <option value="<?= $k['id'] ?>" <?= $k['id'] == $anggota['cabang'] ?>><?= $k['keterangan'] ?></option>
                <?php } ?>
                </optgroup>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <?= form_label('Tingkat', 'tingkat', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10 mt-2">
              <?php foreach ($tingkat as $t) { ?>
              <div class="icheck-primary d-inline">
                <?php echo form_radio('tingkat', $t, $t == $anggota['tingkat'], ['class' => 'form-check-input', 'id' => 'tingkat_' . $t]); echo form_label($t, 'tingkat_' . $t, ['class' => 'form-check-label']) ?>
              </div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group row">
            <?= form_label('No. Izin Konsultan Pajak', 'no_izin_konsultan', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10"><?= form_input('no_izin_konsultan', $anggota['no_izin_konsultan'], ['class' => 'form-control']); ?></div>
          </div>
          <div class="form-group row">
            <?= form_label('NIK', 'nik', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10"><?= form_input('nik', $anggota['nik'], ['class' => 'form-control']); ?></div>
          </div>
          <div class="form-group row">
            <?= form_label('NPWP', 'npwp', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10">
              <?= form_input('npwp', $anggota['npwp'], ['class' => 'form-control inputmask', 'data-inputmask' => "'mask':'99.999.999.9-999.999'"]) ?>
            </div>
          </div>
          <div class="form-group row">
            <?= form_label('Tempat, Tanggal Lahir', 'nama', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-3">
              <select name="tempat_lahir" class="form-control select2">
                <option disabled selected>Pilih Tempat Lahir</option>
                <?php foreach ($area as $a) { ?>
                <optgroup label="<?= $a->keterangan ?>">
                <?php foreach ($a->kota as $k) { ?>
                  <option value="<?= $k['id'] ?>" <?= $k['id'] == $anggota['tempat_lahir'] ? 'selected' : '' ?>><?= $k['keterangan'] ?></option>
                <?php } ?>
                </optgroup>
                <?php } ?>
              </select>
            </div>
            <div class="col-md-3">
              <input type="date" name="tanggal_lahir" class="form-control" max="<?= date('Y-m-d') ?>" value="<?= $anggota['tanggal_lahir'] ?>">
            </div>
          </div>
          <div class="form-group row">
            <?= form_label('Jenis Kelamin', 'jenis_kelamin', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10 mt-2">
              <?php foreach ($jenis_kelamin as $key => $value) { ?>
              <div class="icheck-primary d-inline">
                <?php echo form_radio('jenis_kelamin', $key, $key == $anggota['jenis_kelamin'], ['class' => 'form-check-input', 'id' => 'jk_' . $key]); echo form_label($value, 'jk_' . $key, ['class' => 'form-check-label']) ?>
              </div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group row">
            <?= form_label('Alamat Rumah', 'alamat_rumah', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10"><?= form_textarea('alamat_rumah', $anggota['alamat_rumah'], ['class' => 'form-control', 'rows' => 3]); ?></div>
          </div>
          <div class="form-group row">
            <?= form_label('Alamat Domisili', 'alamat_domisili', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10"><?= form_textarea('alamat_domisili', $anggota['alamat_domisili'], ['class' => 'form-control', 'rows' => 3]); ?></div>
          </div>
          <div class="form-group row">
            <?= form_label('Pekerjaan', 'pekerjaan', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10"><?= form_input('pekerjaan', $anggota['pekerjaan'], ['class' => 'form-control']); ?></div>
          </div>
          <div class="form-group row">
            <?= form_label('Alamat Kantor', 'alamat_kantor', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10"><?= form_textarea('alamat_kantor', $anggota['alamat_kantor'], ['class' => 'form-control', 'rows' => 3]); ?></div>
          </div>
          <div class="form-group row">
            <?= form_label('No. Handphone', 'no_hp', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10"><?= form_input('no_hp', $anggota['no_hp'], ['class' => 'form-control inputmask', 'data-inputmask' => "'mask':['0899999999','08999999999','089999999999','0899999999999']"]); ?></div>
          </div>
          <div class="form-group row">
            <?= form_label('No. Telepon', 'no_telp', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10"><?= form_input('no_telp', $anggota['no_telp'], ['class' => 'form-control inputmask', 'data-inputmask' => "'mask':['0999999999','09999999999','099999999999','0999999999999']"]); ?></div>
          </div>
          <div class="form-group row">
            <?= form_label('Email', 'email', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10"><?= form_input('email', $anggota['email'], ['class' => 'form-control']); ?></div>
          </div>
          <div class="form-group row">
            <?= form_label('Rekomendasi', 'rekomendasi', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10"><?= form_input('rekomendasi', $anggota['rekomendasi'], ['class' => 'form-control']); ?></div>
          </div>
          <div class="form-group row">
            <?= form_label('Pendidikan Terakhir', 'pendidikan_terakhir', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10"><?= form_input('pendidikan_terakhir', $anggota['pendidikan_terakhir'], ['class' => 'form-control']); ?></div>
          </div>
          <div class="form-group row">
            <?= form_label('Alamat Korespondensi', 'alamat_korespondensi', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10"><?= form_dropdown('alamat_korespondensi', $alamat_korespondensi, $anggota['alamat_korespondensi'], ['class' => 'form-control']); ?></div>
          </div>
          <div class="form-group row">
            <?= form_label('Pensiunan DJP', 'nama', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10">
              <div class="icheck-primary d-inline">
                <?php echo form_radio('pensiunan_djp', 1, $anggota['pensiunan_djp'] == 1, ['class' => 'form-check-input', 'id' => 'djp-1']); echo form_label('Ya', 'djp-1', ['class' => 'form-check-label']) ?>
              </div>
              <div class="icheck-primary d-inline">
                <?php echo form_radio('pensiunan_djp', 0, $anggota['pensiunan_djp'] == 1, ['class' => 'form-check-input', 'id' => 'djp-0']); echo form_label('Bukan', 'djp-0', ['class' => 'form-check-label']) ?>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <?= form_label('Status Anggota', 'nama', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10">
              <div class="icheck-success d-inline">
                <?php echo form_radio('status', 1, $anggota['status'] == 1, ['class' => 'form-check-input', 'id' => 'status-aktif']); echo form_label('Aktif', 'status-aktif', ['class' => 'form-check-label text-success']) ?>
              </div>
              <div class="icheck-danger d-inline">
                <?php echo form_radio('status', 0, $anggota['status'] == 0, ['class' => 'form-check-input', 'id' => 'status-nonaktif']); echo form_label('Non-Aktif', 'status-nonaktif', ['class' => 'form-check-label text-danger']) ?>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <?= form_label('Pas Foto', 'nama', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10">
              <?php if (isset($anggota['pas_foto'])) { ?>
                <img src="<?= base_url($anggota['pas_foto']) ?>" width="150">
                <br>
                <a href="#" class="btn btn-danger mt-1" onclick="hapusGambar('<?= $this->uri->segment(3) ?>')"><i class="fas fa-trash"></i> Hapus Gambar</a>
              <?php } else { 
                echo form_upload('pas_foto', '', ['class' => 'form-control-file']);
              } ?>
            </div>
          </div>
          <?= form_submit('submit', 'Simpan', ['class' => 'btn btn-success float-right']); ?>
        </form>
      </div>
    </div>
  </div>
</div>
<script>
  $('.inputmask').inputmask();

  function hapusGambar(id){
    Swal.fire({
      title: "Apakah Anda yakin?",
      text: "Gambar akan terhapus!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Ya",
      cancelButtonText: "Tidak"
    }).then((result) => {
      if (result.value) {
        window.location.href = '<?= base_url('anggota/hapus_foto/') ?>' + id;
      }
    });
  }
</script>