<div class="row">
  <div class="col-md-12">
    <?php if($this->session->flashdata('message')) { ?>
    <div class="alert alert-<?= $this->session->flashdata('color') ?> alert-dismissible mb-3" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <?= $this->session->flashdata('message') ?>
    </div> 
    <?php } ?>
    <div class="mb-2">
      <a href="<?= base_url('anggota') ?>"><i class="fas fa-chevron-circle-left"></i>Kembali ke halaman Anggota</a>
    </div>
    <div class="card">
      <div class="card-body">
        <form action="<?= base_url('anggota/tambah_anggota') ?>" method="post" enctype="multipart/form-data" id="form">
          <div class="form-group row">
            <?= form_label('Nama Anggota', 'nama', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10"><?= form_input('nama', NULL, ['class' => 'form-control']); ?></div>
          </div>
          <div class="form-group row">
            <?= form_label('Jenis Anggota', 'jenis_anggota', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10"><?= form_dropdown('jenis_anggota', $jenis_anggota, '', ['class' => 'form-control']); ?></div>
          </div>
          <div class="form-group row">
            <?= form_label('No. Anggota', 'no_anggota', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10"><?= form_input('no_anggota', NULL, ['class' => 'form-control']); ?></div>
          </div>
          <div class="form-group row">
            <?= form_label('Tanggal Penetapan', 'tanggal_penetapan', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10">
              <input type="date" name="tanggal_penetapan" class="form-control">  
            </div>
          </div>
          <div class="form-group row">
            <?= form_label('Cabang', 'cabang', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10">
              <select name="cabang" class="form-control select2" required>
                <option disabled selected>Pilih Cabang</option>
                <?php foreach ($area as $a) { ?>
                <optgroup label="<?= $a->keterangan ?>">
                <?php foreach ($a->kota as $k) { ?>
                  <option value="<?= $k['id'] ?>"><?= $k['keterangan'] ?></option>
                <?php } ?>
                </optgroup>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <?= form_label('Tingkat', 'tingkat', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10 mt-2">
              <?php foreach ($tingkat as $t) { ?>
              <div class="icheck-primary d-inline">
                <?php echo form_radio('tingkat', $t, FALSE, ['class' => 'form-check-input', 'id' => 'tingkat_' . $t]); echo form_label($t, 'tingkat_' . $t, ['class' => 'form-check-label']) ?>
              </div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group row">
            <?= form_label('No. Izin Konsultan Pajak', 'no_izin_konsultan', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10"><?= form_input('no_izin_konsultan', NULL, ['class' => 'form-control']); ?></div>
          </div>
          <div class="form-group row">
            <?= form_label('NIK', 'nik', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10"><?= form_input('nik', NULL, ['class' => 'form-control']); ?></div>
          </div>
          <div class="form-group row">
            <?= form_label('NPWP', 'npwp', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10">
              <?= form_input('npwp', NULL, ['class' => 'form-control inputmask', 'data-inputmask' => "'mask':'99.999.999.9-999.999'"]) ?>
            </div>
          </div>
          <div class="form-group row">
            <?= form_label('Tempat, Tanggal Lahir', 'nama', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-3">
              <select name="tempat_lahir" class="form-control select2">
                <option disabled selected>Pilih Tempat Lahir</option>
                <?php foreach ($area as $a) { ?>
                <optgroup label="<?= $a->keterangan ?>">
                <?php foreach ($a->kota as $k) { ?>
                  <option value="<?= $k['id'] ?>"><?= $k['keterangan'] ?></option>
                <?php } ?>
                </optgroup>
                <?php } ?>
              </select>
            </div>
            <div class="col-md-3">
              <input type="date" name="tanggal_lahir" class="form-control" max="<?= date('Y-m-d') ?>">
            </div>
          </div>
          <div class="form-group row">
            <?= form_label('Jenis Kelamin', 'jenis_kelamin', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10 mt-2">
              <?php foreach ($jenis_kelamin as $key => $value) { ?>
              <div class="icheck-primary d-inline">
                <?php echo form_radio('jenis_kelamin', $key, FALSE, ['class' => 'form-check-input', 'id' => 'jk_' . $key]); echo form_label($value, 'jk_' . $key, ['class' => 'form-check-label']) ?>
              </div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group row">
            <?= form_label('Alamat Rumah', 'alamat_rumah', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10"><?= form_textarea('alamat_rumah', NULL, ['class' => 'form-control', 'rows' => 3]); ?></div>
          </div>
          <div class="form-group row">
            <?= form_label('Alamat Domisili', 'alamat_domisili', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10"><?= form_textarea('alamat_domisili', NULL, ['class' => 'form-control', 'rows' => 3]); ?></div>
          </div>
          <div class="form-group row">
            <?= form_label('Pekerjaan', 'pekerjaan', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10"><?= form_input('pekerjaan', NULL, ['class' => 'form-control']); ?></div>
          </div>
          <div class="form-group row">
            <?= form_label('Alamat Kantor', 'alamat_kantor', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10"><?= form_textarea('alamat_kantor', NULL, ['class' => 'form-control', 'rows' => 3]); ?></div>
          </div>
          <div class="form-group row">
            <?= form_label('No. Handphone', 'no_hp', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10"><?= form_input('no_hp', NULL, ['class' => 'form-control inputmask', 'data-inputmask' => "'mask':['0899999999','08999999999','089999999999','0899999999999']"]); ?></div>
          </div>
          <div class="form-group row">
            <?= form_label('No. Telepon', 'no_telp', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10"><?= form_input('no_telp', NULL, ['class' => 'form-control inputmask', 'data-inputmask' => "'mask':['0999999999','09999999999','099999999999','0999999999999']"]); ?></div>
          </div>
          <div class="form-group row">
            <?= form_label('Email', 'email', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10"><?= form_input('email', NULL, ['class' => 'form-control']); ?></div>
          </div>
          <div class="form-group row">
            <?= form_label('Rekomendasi', 'rekomendasi', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10"><?= form_input('rekomendasi', NULL, ['class' => 'form-control']); ?></div>
          </div>
          <div class="form-group row">
            <?= form_label('Pendidikan Terakhir', 'pendidikan_terakhir', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10"><?= form_input('pendidikan_terakhir', NULL, ['class' => 'form-control']); ?></div>
          </div>
          <div class="form-group row">
            <?= form_label('Alamat Korespondensi', 'alamat_korespondensi', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10"><?= form_dropdown('alamat_korespondensi', $alamat_korespondensi, '', ['class' => 'form-control']); ?></div>
          </div>
          <div class="form-group row">
            <?= form_label('Pensiunan DJP', 'nama', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10">
              <div class="icheck-primary d-inline">
                <?php echo form_radio('pensiunan_djp', 1, FALSE, ['class' => 'form-check-input', 'id' => 'djp-1']); echo form_label('Ya', 'djp-1', ['class' => 'form-check-label']) ?>
              </div>
              <div class="icheck-primary d-inline">
                <?php echo form_radio('pensiunan_djp', 0, FALSE, ['class' => 'form-check-input', 'id' => 'djp-0']); echo form_label('Bukan', 'djp-0', ['class' => 'form-check-label']) ?>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <?= form_label('Status Anggota', 'nama', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10">
              <div class="icheck-success d-inline">
                <?php echo form_radio('status', 1, FALSE, ['class' => 'form-check-input', 'id' => 'status-aktif']); echo form_label('Aktif', 'status-aktif', ['class' => 'form-check-label text-success']) ?>
              </div>
              <div class="icheck-danger d-inline">
                <?php echo form_radio('status', 0, FALSE, ['class' => 'form-check-input', 'id' => 'status-nonaktif']); echo form_label('Non-Aktif', 'status-nonaktif', ['class' => 'form-check-label text-danger']) ?>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <?= form_label('Pas Foto', 'nama', ['class' => 'col-md-2 col-form-label']); ?>
            <div class="col-md-10"><?= form_upload('foto', NULL, ['class' => 'form-control-file']) ?></div>
          </div>
          <?php echo form_submit('submit', 'Simpan & Tambah Lagi', ['class' => 'btn btn-success float-left']);
          echo form_submit('submit', 'Simpan', ['class' => 'btn btn-success float-right']); ?>
        </form>
      </div>
    </div>
  </div>
</div>
<script>
  $('.inputmask').inputmask();
</script>