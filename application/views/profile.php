<div class="row">
  <div class="col-md-12">
    <?php if($this->session->flashdata('message')) { ?>
    <div class="alert alert-<?= $this->session->flashdata('color') ?> alert-dismissible mb-3" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <?= $this->session->flashdata('message') ?>
    </div> 
    <?php } ?>
  </div>
  <div class="col-md-4">
    <div class="card">
      <div class="card-body">
        <form action="<?= base_url('user/change_photo') ?>" method="POST" enctype="multipart/form-data">
          <img id="preview" class="ml-auto mr-auto d-block" src="<?= base_url($this->session->userdata('photo') ? $this->session->userdata('photo') : 'assets/dist/img/user2-160x160.jpg') ?>" alt="user" width="150">
          <input type="file" name="profile" id="image" style="display: none;">
          <div class="row mt-2">
            <div class="mx-auto">
              <a class="btn btn-sm btn-default" href="javascript:changeProfile()">Ubah</a>
              <a class="btn btn-sm btn-danger" href="javascript:removeImage()">Hapus</a>
            </div>
          </div>
          <h3 class="text-center mt-3"><?= $this->session->userdata('name') ?></h3>
          <button type="submit" class="btn btn-primary btn-block">Ubah Foto</button>
        </form>
      </div>
    </div>
  </div>
  <div class="col-md-8">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Ubah Profil Pengguna</h4>
      </div>
      <div class="card-body">
      <?= $form ?>
      </div>
    </div>
  </div>
</div>
<script>
function changeProfile() {
  $('#image').click();
}

$('#image').change(function () {
  var imgPath = this.value;
  var ext = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
  if (ext == "gif" || ext == "png" || ext == "jpg" || ext == "jpeg")
    readURL(this);
  else
    alert("Please select image file (jpg, jpeg, png).")
});

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.readAsDataURL(input.files[0]);
    reader.onload = function (e) {
      $('#preview').attr('src', e.target.result);
    };
  }
}

function removeImage() {
  $('#preview').attr('src', '<?= base_url('assets/dist/img/user2-160x160.jpg') ?>');
}
</script>