<div class="row">
  <div class="col-lg-3 col-6">
    <div class="small-box bg-success">
      <div class="inner">
        <h3><?= $anggota_aktif ?></h3>
        <p>Anggota Aktif</p>
      </div>
      <div class="icon">
        <i class="fas fa-user"></i>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-6">
    <div class="small-box bg-danger">
      <div class="inner">
        <h3><?= $anggota_nonaktif ?></h3>
        <p>Anggota Nonaktif</p>
      </div>
      <div class="icon">
        <i class="fas fa-user-slash"></i>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-6">
    <div class="small-box bg-info">
      <div class="inner">
        <h3><?= $jumlah_anggota ?></h3>
        <p>Jumlah Anggota</p>
      </div>
      <div class="icon">
        <i class="fas fa-users"></i>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-6">
    <div class="small-box bg-warning">
      <div class="inner">
        <h3><?= $jumlah_ppl ?></h3>
        <p>Jumlah PPL</p>
      </div>
      <div class="icon">
        <i class="fas fa-book-open"></i>
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <h3 class="m-0 mb-3 text-dark">Rekapitulasi Data Anggota</h3>
  </div>
  <div class="col-md-6">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Berdasarkan Jenis Anggota</h4>
      </div>
      <div class="card-body">
        <div class="table-responsive-md">
          <table class="table table-bordered table-striped table-hover">
            <thead>
              <tr>
                <th class="text-center">Jenis Anggota</th>
                <th class="text-center">Jumlah Anggota</th>
              </tr>
            </thead>
            <tbody>
            <?php foreach ($jumlah_anggota_by_jenis as $key => $value) { ?>
              <tr>
                <td class="text-center"><?= $key ?></td>
                <td class="text-center"><?= $value ?></td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Berdasarkan Jenis Kelamin</h4>
      </div>
      <div class="card-body">
        <div class="table-responsive-md">
          <table class="table table-bordered table-striped table-hover">
            <thead>
              <tr>
                <th class="text-center">Jenis Kelamin</th>
                <th class="text-center">Jumlah Anggota</th>
              </tr>
            </thead>
            <tbody>
            <?php foreach ($jumlah_anggota_by_jk as $key => $value) { ?>
              <tr>
                <td class="text-center"><?= $key ?></td>
                <td class="text-center"><?= $value ?></td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Berdasarkan Cabang</h4>
      </div>
      <div class="card-body">
        <div class="table-responsive-md">
          <table class="table table-bordered table-striped table-hover datatable">
            <thead>
              <tr>
                <th class="text-center">Cabang</th>
                <th class="text-center">Jumlah Anggota</th>
              </tr>
            </thead>
            <tbody>
            <?php foreach ($jumlah_anggota_by_cabang as $ja) { ?>
              <tr>
                <td><?= $ja['keterangan'] ?></td>
                <td class="text-center"><?= $ja['jumlah'] ?></td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Berdasarkan Rentang Usia</h4>
      </div>
      <div class="card-body">
        <div class="table-responsive-md">
          <table class="table table-bordered table-striped table-hover">
            <thead>
              <tr>
                <th class="text-center">Rentang Usia</th>
                <th class="text-center">Jumlah Anggota</th>
              </tr>
            </thead>
            <tbody>
            <?php foreach ($jumlah_anggota_by_range_usia as $key => $value) { ?>
              <tr>
                <td class="text-center"><?= $key ?></td>
                <td class="text-center"><?= $value ?></td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>