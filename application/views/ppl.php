<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <a href="<?= base_url('ppl/tambah') ?>" class="btn btn-success"><i class="fas fa-plus-circle"></i> Tambah Data</a>
        <div class="table-responsive-md mt-3">
          <table class="table table-bordered table-striped table-hover datatable">
            <thead>
              <tr>
                <th class="text-center">No.</th>
                <th class="text-center">Topik</th>
                <th class="text-center">Penyelenggara</th>
                <th class="text-center">Tanggal</th>
                <th class="text-center">Tempat</th>
                <th class="text-center">Narasumber</th>
                <th class="text-center">Jumlah Peserta</th>
                <th class="text-center">SKPPL</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
            <?php $i = 1; foreach ($ppl as $p) { ?>
              <tr>
                <td class="text-center"><?= $i ?></td>
                <td><?= $p['topik'] ?></td>
                <td><?= $p['penyelenggara'] ?></td>
                <td><?= dateIndo($p['tanggal']) ?></td>
                <td><?= $p['tempat'] ?></td>
                <td><?= $p['narasumber'] ?></td>
                <td class="text-center"><?= $p['jumlah_peserta'] ?></td>
                <td class="text-center"><?= $p['skppl'] ?></td>
                <td class="text-center">
                  <!-- <a href="#" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fas fa-eye"></i></a> -->
                  <a href="<?= base_url('ppl/edit/' . $p['id']) ?>" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-pencil-alt"></i></a>
                </td>
              </tr>
            <?php $i++; } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>