<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?= $title ?></title>

  <style>
    @font-face {
      font-family: 'Helvetica';
      font-weight: normal;
      font-style: normal;
      font-variant: normal;
      src: local("Helvetica") format("truetype");
    }
    body {
      font-family: 'Helvetica';
      font-size: 13px;
      margin: 50px;
    }

    .kop {
      text-align: center;
    }

    .identitas-konsultan {
      margin-top: 10px;
    }

    .realisasi-ppl {
      margin-top: 15px;
    }

    .text-center {
      text-align: center;
    }

    .realisasi-ppl table, .realisasi-ppl th, .realisasi-ppl td {
      border: 1px solid black;
      border-collapse: collapse;
    }

    .realisasi-ppl th, .realisasi-ppl td {
      padding: 4px;
    }

    .tanda-tangan {
      float: right;
      margin-right: 10px;
      margin-top: 15px;
      text-align: center;
    }
  </style>
</head>
<body>
  <div class="kop">
    <strong>DAFTAR REALISASI</strong>
    <strong>KEGIATAN PENGEMBANGAN PROFESIONAL BERKELANJUTAN (PPL)</strong>
    <?= $this->uri->segment(4) != 'all' ? '<strong>TAHUN ' . $this->uri->segment(4) . '</strong><br>' : '' ?><br>
    <strong>(Peraturan Direktur Jenderal Pajak Nomor PER-13/PJ/2015 Tentang Petunjuk Pelaksanaan Administrasi Konsultan Pajak)</strong>
  </div>
  <div class="identitas-konsultan">
    <table>
      <tr>
        <td>Nama Konsultan Pajak</td>
        <td>:</td>
        <td><?= $anggota['nama'] ?></td>
      </tr>
      <tr>
        <td>Nomor Surat Izin Praktik</td>
        <td>:</td>
        <td><?= $anggota['no_izin_konsultan'] ?></td>
      </tr>
      <tr>
        <td>Tingkat</td>
        <td>:</td>
        <td><?= $anggota['tingkat'] ?></td>
      </tr>
    </table>
  </div>
  <div class="realisasi-ppl">
    <table style="width: 100%;">
      <thead>
        <tr>
          <th class="text-center">No.</th>
          <th class="text-center">Nama Kegiatan</th>
          <th class="text-center">Tanggal</th>
          <th class="text-center">Tempat</th>
          <th class="text-center">Penyelenggara</th>
          <th class="text-center">Jumlah SKPPL</th>
        </tr>
      </thead>
      <tbody id="ppl_body">
        <tr>
          <td colspan="6">Kegiatan PPL Terstruktur</td>
        </tr>
        <?php $i = 1; foreach ($realisasi_ppl['Terstruktur'] as $rp) { ?>
        <tr>
          <td class="text-center"><?= $i ?></td>
          <td><?= $rp['nama_kegiatan'] ?></td>
          <td><?= dateIndo($rp['tanggal']) ?></td>
          <td><?= $rp['tempat'] ?></td>
          <td><?= $rp['penyelenggara'] ?></td>
          <td class="text-center"><?= $rp['jumlah_skppl'] ?></td>
        </tr>
        <?php $i++; } ?>
        <tr>
          <td colspan="6">Kegiatan PPL Tidak Terstruktur</td>
        </tr>
        <?php $i = 1; foreach ($realisasi_ppl['Tidak Terstruktur'] as $rp) { ?>
        <tr>
          <td class="text-center"><?= $i ?></td>
          <td><?= $rp['nama_kegiatan'] ?></td>
          <td><?= dateIndo($rp['tanggal']) ?></td>
          <td><?= $rp['tempat'] ?></td>
          <td><?= $rp['penyelenggara'] ?></td>
          <td class="text-center"><?= $rp['jumlah_skppl'] ?></td>
        </tr>
        <?php $i++; } ?>
        <tr>
          <td class="text-center" colspan="5"><b>Total SKPPL</b></td>
          <td class="text-center"><?= $jumlah_skppl ?></td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="tanda-tangan">
    Jakarta, <?= dateIndo(date('Y-m-d')) ?><br>
    Pengurus Pusat<br>
    Asosisasi Konsultan Pajak Publik Indonesia<br>
    (AKP2I)
    <br><br><br><br><br><br><br><br>
    <strong><u>Dr. Suherman Saleh, Ak., M. Sc., CA</u></strong><br>
    Ketua Umum
  </div>
</body>
</html>