<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->model(array('M_auth'));
  }
  
  public function index(){
    $this->load->view('templates/login', [
      'title' => 'Login'
    ]);
  }

  public function auth(){
    $username = $this->input->post('username');
    $password = $this->input->post('password');
    $check = $this->M_auth->login($username, $password);
    if ($check == TRUE) {
      redirect('dashboard');
    } else {
      $this->M_app->setAlert('danger', 'Username/Password Salah');
      redirect('login');
    }
  }

  public function logout(){
    $this->session->sess_destroy();
    $this->M_app->setAlert('warning', 'Terima kasih. Sampai jumpa lagi!');
    redirect('login');
  }
}

/* End of file Login.php */
 ?>