<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Anggota extends CI_Controller {

  protected $jenis_anggota;
  protected $tingkat;
  protected $jenis_kelamin;
  protected $alamat_korespondensi;
  protected $jenis_ppl;

  public function __construct(){
    parent::__construct();
    if (!$this->session->has_userdata('id')) redirect('login');
    $this->load->model(array('M_anggota'));
    $this->load->library(array('upload', 'image_lib'));

    $this->jenis_anggota = ['Anggota Utama' => 'Anggota Utama', 'Anggota Madya' => 'Anggota Madya', 'Anggota Pratama' => 'Anggota Pratama', 'Anggota Kehormatan' => 'Anggota Kehormatan'];
    $this->tingkat = ['A', 'B', 'C'];
    $this->jenis_kelamin = ['L' => 'Laki-Laki', 'P' => 'Perempuan'];
    $this->alamat_korespondensi = ['Alamat Rumah' => 'Alamat Rumah', 'Alamat Domisili' => 'Alamat Domisili', 'Alamat Kantor' => 'Alamat Kantor'];
    $this->jenis_ppl = ['Terstruktur', 'Tidak Terstruktur'];
  }

  public function index(){
    $this->load->view('templates/index', [
      'title' => 'Anggota',
      'page_title' => 'Anggota',
      'content' => 'anggota/index',
      'anggota' => $this->M_anggota->get_anggota()->result_array()
    ]);
  }

  public function detail($id) {
    $realisasi_ppl = ['Terstruktur' => [], 'Tidak Terstruktur' => []];
    $data_realisasi_ppl = $this->M_anggota->get_realisasi_ppl_anggota($id)->result_array();
    $jumlah_skppl = 0;
    foreach ($data_realisasi_ppl as $drp) {
      if ($drp['jenis_ppl'] == 'Terstruktur') {
        $realisasi_ppl['Terstruktur'][] = $drp;
      } else {
        $realisasi_ppl['Tidak Terstruktur'][] = $drp;
      }
      $jumlah_skppl += $drp['jumlah_skppl'];
    }

    $this->load->view('templates/index', [
      'title' => 'Profil Anggota',
      'page_title' => 'Profil Anggota',
      'content' => 'anggota/detail',
      'anggota' => $this->M_anggota->get_detail_anggota($id),
      'realisasi_ppl' => $realisasi_ppl,
      'jumlah_skppl' => $jumlah_skppl
    ]);
  }

  public function tambah(){
    $this->load->view('templates/index', [
      'title' => 'Tambah Data Anggota',
      'page_title' => 'Tambah Data Anggota',
      'content' => 'anggota/tambah',
      'jenis_anggota' => $this->jenis_anggota,
      'tingkat' => $this->tingkat,
      'jenis_kelamin' => $this->jenis_kelamin,
      'alamat_korespondensi' => $this->alamat_korespondensi,
      'area' => $this->M_app->dropdown_area()
    ]);
  }

  public function edit($id){
    $this->load->view('templates/index', [
      'title' => 'Edit Data Anggota',
      'page_title' => 'Edit Data Anggota',
      'content' => 'anggota/edit',
      'jenis_anggota' => $this->jenis_anggota,
      'tingkat' => $this->tingkat,
      'jenis_kelamin' => $this->jenis_kelamin,
      'alamat_korespondensi' => $this->alamat_korespondensi,
      'area' => $this->M_app->dropdown_area(),
      'anggota' => $this->M_anggota->get_anggota($id)->row_array()
    ]);
  }

  public function tambah_anggota(){
    $data = [
      'id' => $this->M_app->getLatestId('id', 'tb_anggota'),
      'no_anggota' => $this->input->post('no_anggota'),
      'nama' => $this->input->post('nama'),
      'jenis_anggota' => $this->input->post('jenis_anggota'),
      'tanggal_penetapan' => $this->input->post('tanggal_penetapan'),
      'cabang' => $this->input->post('cabang'),
      'tingkat' => $this->input->post('tingkat'),
      'no_izin_konsultan' => $this->input->post('no_izin_konsultan'),
      'nik' => $this->input->post('nik'),
      'npwp' => $this->input->post('npwp'),
      'tempat_lahir' => $this->input->post('tempat_lahir'),
      'tanggal_lahir' => $this->input->post('tanggal_lahir'),
      'jenis_kelamin' => $this->input->post('jenis_kelamin'),
      'alamat_rumah' => $this->input->post('alamat_rumah'),
      'alamat_domisili' => $this->input->post('alamat_domisili'),
      'pekerjaan' => $this->input->post('pekerjaan'),
      'alamat_kantor' => $this->input->post('alamat_kantor'),
      'no_hp' => $this->input->post('no_hp'),
      'no_telp' => $this->input->post('no_telp'),
      'email' => $this->input->post('email'),
      'rekomendasi' => $this->input->post('rekomendasi'),
      'pendidikan_terakhir' => $this->input->post('pendidikan_terakhir'),
      'alamat_korespondensi' => $this->input->post('alamat_korespondensi'),
      'pensiunan_djp' => $this->input->post('pensiunan_djp'),
      'status' => $this->input->post('status'),
      'created_at' => date('Y-m-d H:i:s')
    ];

    $config = [
      'upload_path' => './uploads/',
      'allowed_types' => 'png|jpg|jpeg|gif|jfif',
      'encrypt_name' => true,
      'max_sizes' => 2048
    ];
    $this->upload->initialize($config);
    list($width, $height, $type, $attr) = getimagesize($_FILES['foto']['tmp_name']);
    if ($width != $height){
      $config['source_image'] = $_FILES['foto']['tmp_name'];
      $config['x_axis'] = ($width-min($width, $height))/2;
      $config['y_axis'] = ($height-min($width, $height))/2;
      $config['maintain_ratio'] = FALSE;
      $config['width'] = min($width, $height);
      $config['height'] = min($width, $height);
      $this->image_lib->initialize($config);
      $this->image_lib->crop();
    }
    if ($this->upload->do_upload('foto')) $data['pas_foto'] = 'uploads/' . $this->upload->data('file_name');
    $proc = $this->M_anggota->insert_anggota($data);
    $this->M_app->setAlertIfSuccessOrFailed($proc, 'Berhasil menambahkan data anggota.', 'Gagal menambahkan data anggota. Terjadi kesalahan.');
    if ($this->input->post('submit') == 'Simpan & Tambah Lagi') {
      redirect('anggota/tambah');
    } else {
      redirect('anggota');
    }
  }

  public function edit_anggota($id){
    $data = [
      'no_anggota' => $this->input->post('no_anggota'),
      'nama' => $this->input->post('nama'),
      'jenis_anggota' => $this->input->post('jenis_anggota'),
      'tanggal_penetapan' => $this->input->post('tanggal_penetapan'),
      'cabang' => $this->input->post('cabang'),
      'tingkat' => $this->input->post('tingkat'),
      'no_izin_konsultan' => $this->input->post('no_izin_konsultan'),
      'nik' => $this->input->post('nik'),
      'npwp' => $this->input->post('npwp'),
      'tempat_lahir' => $this->input->post('tempat_lahir'),
      'tanggal_lahir' => $this->input->post('tanggal_lahir'),
      'jenis_kelamin' => $this->input->post('jenis_kelamin'),
      'alamat_rumah' => $this->input->post('alamat_rumah'),
      'alamat_domisili' => $this->input->post('alamat_domisili'),
      'pekerjaan' => $this->input->post('pekerjaan'),
      'alamat_kantor' => $this->input->post('alamat_kantor'),
      'no_hp' => $this->input->post('no_hp'),
      'no_telp' => $this->input->post('no_telp'),
      'email' => $this->input->post('email'),
      'rekomendasi' => $this->input->post('rekomendasi'),
      'pendidikan_terakhir' => $this->input->post('pendidikan_terakhir'),
      'alamat_korespondensi' => $this->input->post('alamat_korespondensi'),
      'pensiunan_djp' => $this->input->post('pensiunan_djp'),
      'status' => $this->input->post('status'),
      'updated_at' => date('Y-m-d H:i:s')
    ];

    $config = [
      'upload_path' => './uploads/',
      'allowed_types' => 'png|jpg|jpeg|gif|jfif',
      'encrypt_name' => true,
      'max_sizes' => 2048
    ];
    $this->upload->initialize($config);
    list($width, $height, $type, $attr) = getimagesize($_FILES['foto']['tmp_name']);
    if ($width != $height){
      $config['source_image'] = $_FILES['foto']['tmp_name'];
      $config['x_axis'] = ($width-min($width, $height))/2;
      $config['y_axis'] = ($height-min($width, $height))/2;
      $config['maintain_ratio'] = FALSE;
      $config['width'] = min($width, $height);
      $config['height'] = min($width, $height);
      $this->image_lib->initialize($config);
      $this->image_lib->crop();
    }
    if ($this->upload->do_upload('foto')) $data['pas_foto'] = 'uploads/' . $this->upload->data('file_name');
    $proc = $this->M_anggota->update_anggota($id, $data);
    $this->M_app->setAlertIfSuccessOrFailed($proc, 'Berhasil mengubah data anggota.', 'Gagal mengubah data anggota. Terjadi kesalahan.');
    redirect('anggota');
  }

  public function hapus_foto($id) {
    $old_pictures = $this->M_anggota->get_anggota($id)->row_array()['pas_foto'];
    unlink($old_pictures);
    $data = ['pas_foto' => NULL, 'updated_at' => date('Y-m-d H:i:s')];
    $proc = $this->M_anggota->update_anggota($id, $data);
    $this->M_app->setAlertIfSuccessOrFailed($proc, 'Berhasil menghapus foto anggota.', 'Gagal menghapus foto anggota. Terjadi kesalahan.');
    redirect('anggota/edit/' . $id);
  }

  public function hapus_anggota($id) {
    $proc = $this->M_anggota->delete_anggota($id);
    $this->M_app->setAlertIfSuccessOrFailed($proc, 'Berhasil menghapus data anggota.', 'Gagal menghapus data anggota. Terjadi kesalahan.');
    redirect('anggota');
  }

  public function tambah_realisasi_ppl($id) {
    $form = [
      ['label' => 'Tahun Laporan', 'label_width' => 'col-md-2', 'name' => 'tahun_laporan', 'type' => 'number', 'width' => 'col-md-10', 'attributes' => ['class' => 'form-control', 'min' => 1, 'max' => date('Y'), 'required' => true]],
      ['label' => 'Jenis PPL', 'label_width' => 'col-md-2', 'name' => 'jenis_ppl', 'type' => 'radio', 'width' => 'col-md-10', 'inline' => true, 'dataenum' => $this->jenis_ppl, 'attributes' => ['class' => 'form-check-input', 'required' => true]],
      ['label' => 'Nama Kegiatan', 'label_width' => 'col-md-2', 'name' => 'nama_kegiatan', 'type' => 'text', 'width' => 'col-md-10', 'attributes' => ['class' => 'form-control', 'required' => true]],
      ['label' => 'Tanggal', 'label_width' => 'col-md-2', 'name' => 'tanggal', 'type' => 'date', 'width' => 'col-md-10', 'attributes' => ['class' => 'form-control', 'required' => true]],
      ['label' => 'Tempat', 'label_width' => 'col-md-2', 'name' => 'tempat', 'type' => 'text', 'width' => 'col-md-10', 'attributes' => ['class' => 'form-control', 'required' => true]],
      ['label' => 'Penyelenggara', 'label_width' => 'col-md-2', 'name' => 'penyelenggara', 'type' => 'text', 'width' => 'col-md-10', 'attributes' => ['class' => 'form-control', 'required' => true]],
      ['label' => 'Jumlah SKPPL', 'label_width' => 'col-md-2', 'name' => 'jumlah_skppl', 'type' => 'number', 'width' => 'col-md-10', 'attributes' => ['class' => 'form-control', 'required' => true, 'min' => 1]]
    ];

    $this->load->view('templates/index', [
      'form' => create_form('anggota/tambah_data_realisasi_ppl/' . $id, $form, true, true),
      'title' => 'Tambah Data Realisasi PPL',
      'page_title' => 'Tambah Data Realisasi Realisasi PPL',
      'content' => 'forms',
      'back_text' => 'Kembali ke halaman Profil Anggota',
      'base_url' => 'anggota/detail/' . $id
    ]);
  }

  public function tambah_data_realisasi_ppl($id) {
    $data = [
      'id' => $this->M_app->getLatestId('id', 'tb_anggota_realisasi_ppl'),
      'id_anggota' => $id,
      'tahun_laporan' => $this->input->post('tahun_laporan'),
      'jenis_ppl' => $this->input->post('jenis_ppl'),
      'nama_kegiatan' => $this->input->post('nama_kegiatan'),
      'tanggal' => $this->input->post('tanggal'),
      'tempat' => $this->input->post('tempat'),
      'penyelenggara' => $this->input->post('penyelenggara'),
      'jumlah_skppl' => $this->input->post('jumlah_skppl'),
      'created_at' => date('Y-m-d H:i:s')
    ];
    $proc = $this->M_anggota->insert_realisasi_ppl($data);
    $this->M_app->setAlertIfSuccessOrFailed($proc, 'Berhasil menambahkan data realisasi PPL.', 'Gagal menambahkan data realisasi PPL. Terjadi kesalahan.');
    if ($this->input->post('submit') == 'Simpan & Tambah Lagi') {
      redirect('anggota/tambah_realisasi_ppl/' . $id);
    } else {
      redirect('anggota/detail/' . $id);
    }
  }

  public function edit_realisasi_ppl($id, $id_realisasi_ppl) {
    $data = $this->M_anggota->get_realisasi_ppl_anggota($id, $id_realisasi_ppl)->row_array();

    $form = [
      ['label' => 'Tahun Laporan', 'label_width' => 'col-md-2', 'name' => 'tahun_laporan', 'type' => 'number', 'width' => 'col-md-10', 'attributes' => ['class' => 'form-control', 'min' => 1, 'max' => date('Y'), 'required' => true], 'value' => $data['tahun_laporan']],
      ['label' => 'Jenis PPL', 'label_width' => 'col-md-2', 'name' => 'jenis_ppl', 'type' => 'radio', 'width' => 'col-md-10', 'inline' => true, 'dataenum' => $this->jenis_ppl, 'attributes' => ['class' => 'form-check-input', 'required' => true], 'value' => $data['jenis_ppl']],
      ['label' => 'Nama Kegiatan', 'label_width' => 'col-md-2', 'name' => 'nama_kegiatan', 'type' => 'text', 'width' => 'col-md-10', 'attributes' => ['class' => 'form-control', 'required' => true], 'value' => $data['nama_kegiatan']],
      ['label' => 'Tanggal', 'label_width' => 'col-md-2', 'name' => 'tanggal', 'type' => 'date', 'width' => 'col-md-10', 'attributes' => ['class' => 'form-control', 'required' => true], 'value' => $data['tanggal']],
      ['label' => 'Tempat', 'label_width' => 'col-md-2', 'name' => 'tempat', 'type' => 'text', 'width' => 'col-md-10', 'attributes' => ['class' => 'form-control', 'required' => true], 'value' => $data['tempat']],
      ['label' => 'Penyelenggara', 'label_width' => 'col-md-2', 'name' => 'penyelenggara', 'type' => 'text', 'width' => 'col-md-10', 'attributes' => ['class' => 'form-control', 'required' => true], 'value' => $data['penyelenggara']],
      ['label' => 'Jumlah SKPPL', 'label_width' => 'col-md-2', 'name' => 'jumlah_skppl', 'type' => 'number', 'width' => 'col-md-10', 'attributes' => ['class' => 'form-control', 'required' => true, 'min' => 1], 'value' => $data['jumlah_skppl']]
    ];

    $this->load->view('templates/index', [
      'form' => create_form('anggota/edit_data_realisasi_ppl/' . $id . '/' . $id_realisasi_ppl, $form, false, true),
      'title' => 'Edit Data Realisasi PPL',
      'page_title' => 'Edit Data Realisasi Realisasi PPL',
      'content' => 'forms',
      'back_text' => 'Kembali ke halaman Profil Anggota',
      'base_url' => 'anggota/detail/' . $id
    ]);
  }

  public function edit_data_realisasi_ppl($id, $id_realisasi_ppl) {
    $data = [
      'tahun_laporan' => $this->input->post('tahun_laporan'),
      'jenis_ppl' => $this->input->post('jenis_ppl'),
      'nama_kegiatan' => $this->input->post('nama_kegiatan'),
      'tanggal' => $this->input->post('tanggal'),
      'tempat' => $this->input->post('tempat'),
      'penyelenggara' => $this->input->post('penyelenggara'),
      'jumlah_skppl' => $this->input->post('jumlah_skppl'),
      'updated_at' => date('Y-m-d H:i:s')
    ];
    $proc = $this->M_anggota->update_realisasi_ppl($id_realisasi_ppl, $data);
    $this->M_app->setAlertIfSuccessOrFailed($proc, 'Berhasil mengubah data realisasi PPL.', 'Gagal mengubah data realisasi PPL. Terjadi kesalahan.');
    redirect('anggota/detail/' . $id);
  }

  public function hapus_data_realisasi_ppl($id, $id_realisasi_ppl) {
    $proc = $this->M_anggota->delete_realisasi_ppl($id_realisasi_ppl);
    $this->M_app->setAlertIfSuccessOrFailed($proc, 'Berhasil menghapus data realisasi PPL.', 'Gagal menghapus data realisasi PPL. Terjadi kesalahan.');
    redirect('anggota/detail/' . $id);
  }

  public function get_data_realisasi_ppl($id, $tahun) {
    $realisasi_ppl = ['Terstruktur' => [], 'Tidak Terstruktur' => []];
    $data_realisasi_ppl = $this->M_anggota->get_realisasi_ppl_anggota_tahun($id, $tahun)->result_array();
    $jumlah_skppl = 0;
    foreach ($data_realisasi_ppl as $drp) {
      if ($drp['jenis_ppl'] == 'Terstruktur') {
        $realisasi_ppl['Terstruktur'][] = $drp;
      } else {
        $realisasi_ppl['Tidak Terstruktur'][] = $drp;
      }
      $jumlah_skppl += $drp['jumlah_skppl'];
    }

    $table = '<tr><td colspan="8">Kegiatan PPL Terstruktur</td></tr>';
    $i = 1; 
    foreach ($realisasi_ppl['Terstruktur'] as $rp) {
      $table .= '<tr><td class="text-center">' . $i . '</td><td class="text-center">' . $rp['tahun_laporan'] . '</td><td>' . $rp['nama_kegiatan'] . '</td><td>' . dateIndo($rp['tanggal']) . '</td><td>' . $rp['tempat'] . '</td><td>' . $rp['penyelenggara'] . '</td><td class="text-center">' . $rp['jumlah_skppl'] . '</td>';
      $table .= '<td class="text-center">';
      $table .= '<a href="' . base_url('anggota/edit_realisasi_ppl/' . $id . '/' . $rp['id']) . '" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-pencil-alt"></i></a> ';
      $table .= '<a href="#" class="btn btn-danger" onclick="hapusData(\'' . $rp['id'] .'\')" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fas fa-trash-alt"></i></a>';
      $table .= '</td></tr>';
      $i++; 
    }
    $table .= '<tr><td colspan="8">Kegiatan PPL Tidak Terstruktur</td></tr>';
    $j = 1;
    foreach ($realisasi_ppl['Tidak Terstruktur'] as $rp) {
      $table .= '<tr><td class="text-center">' . $j . '</td><td class="text-center">' . $rp['tahun_laporan'] . '</td><td>' . $rp['nama_kegiatan'] . '</td><td>' . dateIndo($rp['tanggal']) . '</td><td>' . $rp['tempat'] . '</td><td>' . $rp['penyelenggara'] . '</td><td class="text-center">' . $rp['jumlah_skppl'] . '</td>';
      $table .= '<td class="text-center">';
      $table .= '<a href="' . base_url('anggota/edit_realisasi_ppl/' . $id . '/' . $rp['id']) . '" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-pencil-alt"></i></a> ';
      $table .= '<a href="#" class="btn btn-danger" onclick="hapusData(\'' . $rp['id'] .'\')" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fas fa-trash-alt"></i></a>';
      $table .= '</td></tr>';
      $j++; 
    }
    $table .= '<tr><td class="text-center" colspan="6">Total SKPPL</td><td class="text-center" colspan="2">' . $jumlah_skppl . '</td></tr>';
    echo $table;
  }

  public function cetak_ppl($id, $tahun) {
    $realisasi_ppl = ['Terstruktur' => [], 'Tidak Terstruktur' => []];
    $data_realisasi_ppl = $this->M_anggota->get_realisasi_ppl_anggota_tahun($id, $tahun)->result_array();
    $jumlah_skppl = 0;
    foreach ($data_realisasi_ppl as $drp) {
      if ($drp['jenis_ppl'] == 'Terstruktur') {
        $realisasi_ppl['Terstruktur'][] = $drp;
      } else {
        $realisasi_ppl['Tidak Terstruktur'][] = $drp;
      }
      $jumlah_skppl += $drp['jumlah_skppl'];
    }

    $this->load->library('pdf');

    $this->pdf->setPaper('A4', 'landscape');
    $this->pdf->load_view('print_pdf', 'Laporan Daftar Realisasi Kegiatan PPL-' . date('dmYHis') . '.pdf', [
      'title' => 'Print Data Realisasi PPL',
      'anggota' => $this->M_anggota->get_detail_anggota($id),
      'realisasi_ppl' => $realisasi_ppl,
      'jumlah_skppl' => $jumlah_skppl
    ]);
  }
}

/* End of file Dashboard.php */
?>