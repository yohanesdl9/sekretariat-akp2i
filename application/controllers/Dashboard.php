<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

  public function __construct(){
    parent::__construct();
    if (!$this->session->has_userdata('id')) redirect('login');
    $this->load->model(array('M_anggota', 'M_ppl', 'M_rekapitulasi'));
  }
  
  public function index(){
    $this->load->view('templates/index', [
      'title' => 'Dashboard',
      'page_title' => 'Dashboard',
      'content' => 'dashboard',
      'anggota_aktif' => $this->M_anggota->get_anggota_by_status('1')->num_rows(),
      'anggota_nonaktif' => $this->M_anggota->get_anggota_by_status('0')->num_rows(),
      'jumlah_anggota' => $this->M_anggota->get_anggota_by_status()->num_rows(),
      'jumlah_ppl' => $this->M_ppl->get_ppl()->num_rows(),
      'jumlah_anggota_by_jenis' => $this->M_rekapitulasi->get_all_anggota_by_jenis_anggota(),
      'jumlah_anggota_by_jk' => $this->M_rekapitulasi->get_all_anggota_by_jenis_kelamin(),
      'jumlah_anggota_by_cabang' => $this->M_rekapitulasi->get_anggota_by_cabang(),
      'jumlah_anggota_by_range_usia' => $this->M_rekapitulasi->get_all_anggota_by_range_usia()
    ]);
  }

}

/* End of file Dashboard.php */
?>