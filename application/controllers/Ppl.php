<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Ppl extends CI_Controller {

  public function __construct(){
    parent::__construct();
    if (!$this->session->has_userdata('id')) redirect('login');
    $this->load->model(array('M_ppl'));
  }
  
  public function index(){
    $this->load->view('templates/index', [
      'title' => 'PPL',
      'page_title' => 'Data PPL',
      'content' => 'ppl',
      'ppl' => $this->M_ppl->get_ppl()->result_array()
    ]);
  }

  public function tambah(){
    $form = [
      ['label' => 'Topik', 'label_width' => 'col-md-1', 'name' => 'topik', 'type' => 'text', 'width' => 'col-md-11', 'attributes' => ['class' => 'form-control', 'required' => true]],
      ['label' => 'Penyelenggara', 'label_width' => 'col-md-1', 'name' => 'penyelenggara', 'type' => 'text', 'width' => 'col-md-11', 'attributes' => ['class' => 'form-control', 'required' => true]],
      ['label' => 'Tanggal', 'label_width' => 'col-md-1', 'name' => 'tanggal', 'type' => 'date', 'width' => 'col-md-11', 'attributes' => ['class' => 'form-control', 'required' => true]],
      ['label' => 'Tempat', 'label_width' => 'col-md-1', 'name' => 'tempat', 'type' => 'text', 'width' => 'col-md-11', 'attributes' => ['class' => 'form-control', 'required' => true]],
      ['label' => 'Narasumber', 'label_width' => 'col-md-1', 'name' => 'narasumber', 'type' => 'text', 'width' => 'col-md-11', 'attributes' => ['class' => 'form-control', 'required' => true]],
      ['label' => 'Jumlah Peserta', 'label_width' => 'col-md-1', 'name' => 'jumlah_peserta', 'type' => 'number', 'width' => 'col-md-11', 'attributes' => ['class' => 'form-control', 'required' => true, 'min' => 0]],
      ['label' => 'SKPPL', 'label_width' => 'col-md-1', 'name' => 'skppl', 'type' => 'number', 'width' => 'col-md-11', 'attributes' => ['class' => 'form-control', 'required' => true, 'min' => 1], 'value' => 1],
    ];
    $this->load->view('templates/index', [
      'form' => create_form('ppl/tambah_ppl', $form, true, true),
      'title' => 'Tambah Data PPL',
      'page_title' => 'Tambah Data PPL',
      'content' => 'forms',
      'back_text' => 'Kembali ke halaman PPL',
      'base_url' => 'ppl'
    ]);
  }

  public function tambah_ppl(){
    $data = [
      'id' => $this->M_app->getLatestId('id', 'tb_ppl'),
      'topik' => $this->input->post('topik'),
      'penyelenggara' => $this->input->post('penyelenggara'),
      'tanggal' => $this->input->post('tanggal'),
      'tempat' => $this->input->post('tempat'),
      'narasumber' => $this->input->post('narasumber'),
      'jumlah_peserta' => $this->input->post('jumlah_peserta'),
      'skppl' => $this->input->post('skppl'),
      'created_at' => date('Y-m-d H:i:s')
    ];
    $proc = $this->M_ppl->insert_ppl($data);
    $this->M_app->setAlertIfSuccessOrFailed($proc, 'Berhasil menambahkan data PPL', 'Gagal menambahkan data PPL. Terjadi kesalahan');
    if ($this->input->post('submit') == 'Simpan & Tambah Lagi') {
      redirect('ppl/tambah');
    } else {
      redirect('ppl');
    }
  }

  public function edit($id){
    $data = $this->M_ppl->get_ppl($id)->row_array();
    $form = [
      ['label' => 'Topik', 'label_width' => 'col-md-1', 'name' => 'topik', 'type' => 'text', 'width' => 'col-md-11', 'attributes' => ['class' => 'form-control', 'required' => true], 'value' => $data['topik']],
      ['label' => 'Penyelenggara', 'label_width' => 'col-md-1', 'name' => 'penyelenggara', 'type' => 'text', 'width' => 'col-md-11', 'attributes' => ['class' => 'form-control', 'required' => true], 'value' => $data['penyelenggara']],
      ['label' => 'Tanggal', 'label_width' => 'col-md-1', 'name' => 'tanggal', 'type' => 'date', 'width' => 'col-md-11', 'attributes' => ['class' => 'form-control', 'required' => true], 'value' => $data['tanggal']],
      ['label' => 'Tempat', 'label_width' => 'col-md-1', 'name' => 'tempat', 'type' => 'text', 'width' => 'col-md-11', 'attributes' => ['class' => 'form-control', 'required' => true], 'value' => $data['tempat']],
      ['label' => 'Narasumber', 'label_width' => 'col-md-1', 'name' => 'narasumber', 'type' => 'text', 'width' => 'col-md-11', 'attributes' => ['class' => 'form-control', 'required' => true], 'value' => $data['narasumber']],
      ['label' => 'Jumlah Peserta', 'label_width' => 'col-md-1', 'name' => 'jumlah_peserta', 'type' => 'number', 'width' => 'col-md-11', 'attributes' => ['class' => 'form-control', 'required' => true, 'min' => 0], 'value' => $data['jumlah_peserta']],
      ['label' => 'SKPPL', 'label_width' => 'col-md-1', 'name' => 'skppl', 'type' => 'number', 'width' => 'col-md-11', 'attributes' => ['class' => 'form-control', 'required' => true, 'min' => 1], 'value' => $data['skppl']],
    ];
    $this->load->view('templates/index', [
      'form' => create_form('ppl/edit_ppl/' . $id, $form, false, true),
      'title' => 'Edit Data PPL',
      'page_title' => 'Edit Data PPL',
      'content' => 'forms',
      'back_text' => 'Kembali ke halaman PPL',
      'base_url' => 'ppl'
    ]);
  }

  public function edit_ppl($id){
    $data = [
      'topik' => $this->input->post('topik'),
      'penyelenggara' => $this->input->post('penyelenggara'),
      'tanggal' => $this->input->post('tanggal'),
      'tempat' => $this->input->post('tempat'),
      'narasumber' => $this->input->post('narasumber'),
      'jumlah_peserta' => $this->input->post('jumlah_peserta'),
      'skppl' => $this->input->post('skppl'),
      'updated_at' => date('Y-m-d H:i:s')
    ];
    $proc = $this->M_ppl->update_ppl($id, $data);
    $this->M_app->setAlertIfSuccessOrFailed($proc, 'Berhasil mengubah data PPL', 'Gagal mengubah data PPL. Terjadi kesalahan');
    redirect('ppl');
  }
}

/* End of file Ppl.php */
 ?>