<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->model(array('M_auth'));
  }

  public function index() {
    $user = $this->M_auth->get_current_user();
    $form = [
      ['label' => 'Nama User', 'label_width' => 'col-md-2', 'name' => 'name', 'type' => 'text', 'width' => 'col-md-10', 'attributes' => ['class' => 'form-control', 'required' => true], 'value' => $user['name']],
      ['label' => 'Username', 'label_width' => 'col-md-2', 'name' => 'username', 'type' => 'text', 'width' => 'col-md-10', 'attributes' => ['class' => 'form-control', 'required' => true], 'value' => $user['username']],
      ['label' => 'Password', 'label_width' => 'col-md-2', 'name' => 'password', 'type' => 'password', 'width' => 'col-md-10', 'attributes' => ['class' => 'form-control', 'placeholder' => 'Kosongkan jika tidak ingin mengubah password']],
    ];
    $this->load->view('templates/index', [
      'form' => create_form('user/change_profile', $form, false, true),
      'title' => 'Pengaturan Pengguna',
      'page_title' => 'Pengaturan Pengguna',
      'content' => 'profile'
    ]);
  }

  public function change_photo(){
    $config = [
      'upload_path' => './uploads/',
      'allowed_types' => 'png|jpg|jpeg|gif|jfif',
      'encrypt_name' => true,
      'max_sizes' => 2048
    ];
    $this->upload->initialize($config);
    list($width, $height, $type, $attr) = getimagesize($_FILES['profile']['tmp_name']);
    if ($width != $height){
      $config['source_image'] = $_FILES['profile']['tmp_name'];
      $config['x_axis'] = ($width-min($width, $height))/2;
      $config['y_axis'] = ($height-min($width, $height))/2;
      $config['maintain_ratio'] = FALSE;
      $config['width'] = min($width, $height);
      $config['height'] = min($width, $height);
      $this->image_lib->initialize($config);
      $this->image_lib->crop();
    }
    $old_files = $this->M_auth->get_current_user()['photo'];
    if ($this->upload->do_upload('profile')){
      $profile_picture = 'uploads/' . $this->upload->data('file_name');
      $data = ['photo' => $profile_picture, 'updated_at' => date('Y-m-d H:i:s')];
      $proc = $this->M_auth->update_profile($data);
      $this->M_app->setAlertIfSuccessOrFailed($proc, 'Berhasil mengubah foto profil pengguna.', 'Gagal mengubah foto profil pengguna.');
      if ($proc == TRUE) {
        $this->session->set_userdata(['photo' => $profile_picture]);
        if (isset($old_files)) unlink($old_files);
      }
    }
    redirect('user');
  }

  public function change_profile(){
    $data = [
      'name' => $this->input->post('name'),
      'username' => $this->input->post('username'),
      'updated_at' => date('Y-m-d H:i:s')
    ];
    if ($this->input->post('password')) $data['password'] = password_hash($this->input->post('password'), PASSWORD_BCRYPT);
    $proc = $this->M_auth->update_profile($data);
    $this->M_app->setAlertIfSuccessOrFailed($proc, 'Berhasil mengubah profil pengguna.', 'Gagal mengubah profil pengguna.');
    if ($proc == TRUE) {
      $this->session->set_userdata([
        'name' => $this->input->post('name'),
        'username' => $this->input->post('username')
      ]);
    }
    redirect('user');
  }
}

/* End of file User.php */
 ?>