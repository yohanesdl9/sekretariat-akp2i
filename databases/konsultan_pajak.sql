/*
 Navicat Premium Data Transfer

 Source Server         : MySQL
 Source Server Type    : MySQL
 Source Server Version : 100414
 Source Host           : localhost:3306
 Source Schema         : konsultan_pajak

 Target Server Type    : MySQL
 Target Server Version : 100414
 File Encoding         : 65001

 Date: 16/07/2021 18:47:29
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cms_users
-- ----------------------------
DROP TABLE IF EXISTS `cms_users`;
CREATE TABLE `cms_users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `photo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cms_users
-- ----------------------------
INSERT INTO `cms_users` VALUES (1, 'Super Administrator', 'superadmin', '$2y$10$EdDZwoCNEJ6GsbwNWDEyA.8ffp9YWb.jGlmnpffZz.DEN7WoyIG1.', NULL, '2021-04-12 21:15:34', '2021-04-15 10:15:01', NULL);

-- ----------------------------
-- Table structure for tb_anggota
-- ----------------------------
DROP TABLE IF EXISTS `tb_anggota`;
CREATE TABLE `tb_anggota`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_anggota` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nama` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `jenis_anggota` enum('Anggota Utama','Anggota Madya','Anggota Pratama','Anggota Kehormatan') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tanggal_penetapan` date NULL DEFAULT NULL,
  `cabang` int(11) NULL DEFAULT NULL,
  `tingkat` enum('A','B','C') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `no_izin_konsultan` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nik` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `npwp` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tempat_lahir` int(11) NULL DEFAULT NULL,
  `tanggal_lahir` date NULL DEFAULT NULL,
  `jenis_kelamin` enum('L','P') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `alamat_rumah` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `alamat_domisili` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `alamat_kantor` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `pekerjaan` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `no_hp` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `no_telp` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `email` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `rekomendasi` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `pendidikan_terakhir` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `alamat_korespondensi` enum('Alamat Rumah','Alamat Domisili','Alamat Kantor') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pensiunan_djp` tinyint(4) NULL DEFAULT NULL,
  `status` tinyint(4) NULL DEFAULT NULL,
  `pas_foto` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_anggota
-- ----------------------------
INSERT INTO `tb_anggota` VALUES (1, 'KA.0159/AU.B/AKP2I/2', 'Regen Silaban', 'Anggota Madya', '2021-04-16', NULL, 'A', '2342342424', '3214122805930000', '32.432.432.3-432.432', NULL, '0000-00-00', 'L', 'Graha Mandiri Lantai 15, Jl. Imam Bonjol No.61', '-', 'Graha Mandiri Lantai 15, Jl. Imam Bonjol No.61', 'dsfdsfsd', '0824524525255', '0252523242', 'contact.akp2i@gmail.com', 'dhsdsfhdsh', 'sdgfsfhddsg', 'Alamat Rumah', 0, 1, 'uploads/f031788a8989b0edb1b5d32c8e55f5f3.png', '2021-04-15 09:31:25', NULL, NULL);

-- ----------------------------
-- Table structure for tb_anggota_realisasi_ppl
-- ----------------------------
DROP TABLE IF EXISTS `tb_anggota_realisasi_ppl`;
CREATE TABLE `tb_anggota_realisasi_ppl`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_anggota` int(11) NULL DEFAULT NULL,
  `tahun_laporan` int(11) NULL DEFAULT NULL,
  `jenis_ppl` enum('Terstruktur','Tidak Terstruktur') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nama_kegiatan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tanggal` date NULL DEFAULT NULL,
  `tempat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `penyelenggara` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `jumlah_skppl` int(11) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_anggota_realisasi_ppl
-- ----------------------------
INSERT INTO `tb_anggota_realisasi_ppl` VALUES (1, 1, 2021, 'Terstruktur', 'fdsggsdfg', '2021-04-16', 'asgsagasf', 'gsagasgd', 2, '2021-04-15 09:32:16', NULL, NULL);
INSERT INTO `tb_anggota_realisasi_ppl` VALUES (2, 1, 2020, 'Tidak Terstruktur', 'fdsggsdfg', '2021-04-23', 'ersgereg', 'fergregerg', 3, '2021-04-15 09:32:37', '2021-04-15 12:59:24', NULL);
INSERT INTO `tb_anggota_realisasi_ppl` VALUES (3, 1, 2020, 'Tidak Terstruktur', 'Webinar', '2020-09-22', 'Online', 'Sekretariat AKP2I', 5, '2021-04-16 14:00:33', NULL, NULL);

-- ----------------------------
-- Table structure for tb_kota
-- ----------------------------
DROP TABLE IF EXISTS `tb_kota`;
CREATE TABLE `tb_kota`  (
  `id` int(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_provinsi` int(3) NULL DEFAULT NULL,
  `kode` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keterangan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `created_user_id` int(3) NULL DEFAULT NULL,
  `created_user` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_user` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deleted_user` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `is_visible` int(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 515 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_kota
-- ----------------------------
INSERT INTO `tb_kota` VALUES (1, 1, 'MBO', 'Kabupaten Aceh Barat', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (2, 1, 'BPD', 'Kabupaten Aceh Barat Daya', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (3, 1, 'JTH', 'Kabupaten Aceh Besar', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (4, 1, 'CAG', 'Kabupaten Aceh Jaya', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (5, 1, 'TTN', 'Kabupaten Aceh Selatan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (6, 1, 'SKL', 'Kabupaten Aceh Singkil', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (7, 1, 'KRB', 'Kabupaten Aceh Tamiang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (8, 1, 'TKN', 'Kabupaten Aceh Tengah', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (9, 1, 'KTN', 'Kabupaten Aceh Tenggara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (10, 1, 'LGS', 'Kabupaten Aceh Timur', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (11, 1, 'LSK', 'Kabupaten Aceh Utara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (12, 1, 'STR', 'Kabupaten Bener Meriah', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (13, 1, 'BIR', 'Kabupaten Bireuen', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (14, 1, 'BKJ', 'Kabupaten Gayo Lues', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (15, 1, 'SKM', 'Kabupaten Nagan Raya', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (16, 1, 'SGI', 'Kabupaten Pidie', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (17, 1, 'MRN', 'Kabupaten Pidie Jaya', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (18, 1, 'SNB', 'Kabupaten Simeulue', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (19, 1, 'BNA', 'Kota Banda Aceh', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (20, 1, 'LGS', 'Kota Langsa', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (21, 1, 'LSM', 'Kota Lhokseumawe', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (22, 1, 'SAB', 'Kota Sabang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (23, 1, 'SUS', 'Kota Subulussalam', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (24, 2, 'MGW', 'Kabupaten Badung', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (25, 2, 'BLI', 'Kabupaten Bangli', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (26, 2, 'SGR', 'Kabupaten Buleleng', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (27, 2, 'GIN', 'Kabupaten Gianyar', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (28, 2, 'NGA', 'Kabupaten Jembrana', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (29, 2, 'KRA', 'Kabupaten Karangasem', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (30, 2, 'SRP', 'Kabupaten Klungkung', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (31, 2, 'TAB', 'Kabupaten Tabanan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (32, 2, 'DPR', 'Kota Denpasar', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (33, 3, 'SGL', 'Kabupaten Bangka', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (34, 3, 'MTK', 'Kabupaten Bangka Barat', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (35, 3, 'TBL', 'Kabupaten Bangka Selatan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (36, 3, 'KBA', 'Kabupaten Bangka Tengah', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (37, 3, 'TDN', 'Kabupaten Belitung', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (38, 3, 'MGR', 'Kabupaten Belitung Timur', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (39, 3, 'PGP', 'Kota Pangkal Pinang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (40, 4, 'RKB', 'Kabupaten Lebak', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (41, 4, 'PDG', 'Kabupaten Pandeglang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (42, 4, 'SRG', 'Kabupaten Serang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (43, 4, 'TGR', 'Kabupaten Tangerang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (44, 4, 'CLG', 'Kota Cilegon', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (45, 4, 'SRG', 'Kota Serang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (46, 4, 'TNG', 'Kota Tangerang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (47, 4, 'CPT', 'Kota Tangerang Selatan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (48, 5, 'MNA', 'Kabupaten Bengkulu Selatan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (49, 5, 'KRT', 'Kabupaten Bengkulu Tengah', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (50, 5, 'AGM', 'Kabupaten Bengkulu Utara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (51, 5, 'BHN', 'Kabupaten Kaur', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (52, 5, 'KPH', 'Kabupaten Kepahiang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (53, 5, 'TUB', 'Kabupaten Lebong', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (54, 5, 'MKM', 'Kabupaten Muko Muko', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (55, 5, 'CRP', 'Kabupaten Rejang Lebong', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (56, 5, 'TAS', 'Kabupaten Seluma', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (57, 5, 'BGL', 'Kota Bengkulu', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (58, 6, 'BTL', 'Kabupaten Bantul', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (59, 6, 'WNO', 'Kabupaten Gunung Kidul', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (60, 6, 'WAT', 'Kabupaten Kulon Progo', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (61, 6, 'SMN', 'Kabupaten Sleman', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (62, 6, 'YYK', 'Kota Yogyakarta', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (63, 7, 'KSU', 'Kabupaten Adm. Kepulauan Seribu', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (64, 7, 'GGP', 'Kota Adm. Jakarta Barat', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (65, 7, 'TNA', 'Kota Adm. Jakarta Pusat', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (66, 7, 'KYB', 'Kota Adm. Jakarta Selatan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (67, 7, 'CKG', 'Kota Adm. Jakarta Timur', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (68, 7, 'TJP', 'Kota Adm. Jakarta Utara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (69, 8, 'TMT', 'Kabupaten Boalemo', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (70, 8, 'SWW', 'Kabupaten Bone Bolango', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (71, 8, 'LBT', 'Kabupaten Gorontalo', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (72, 8, 'KWD', 'Kabupaten Gorontalo Utara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (73, 8, 'MAR', 'Kabupaten Pohuwato', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (74, 8, 'GTO', 'Kota Gorontalo', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (75, 9, 'MBN', 'Kabupaten Batanghari', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (76, 9, 'MRB', 'Kabupaten Bungo', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (77, 9, 'SPN', 'Kabupaten Kerinci', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (78, 9, 'BKO', 'Kabupaten Merangin', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (79, 9, 'SNT', 'Kabupaten Muaro Jambi', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (80, 9, 'SRL', 'Kabupaten Sarolangun', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (81, 9, 'KLT', 'Kabupaten Tanjung Jabung Barat', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (82, 9, 'MSK', 'Kabupaten Tanjung Jabung Timur', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (83, 9, 'MRT', 'Kabupaten Tebo', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (84, 9, 'JMB', 'Kota Jambi', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (85, 9, 'SPN', 'Kota Sungai Penuh', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (86, 10, 'SOR', 'Kabupaten Bandung', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (87, 10, 'NPH', 'Kabupaten Bandung Barat', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (88, 10, 'CKR', 'Kabupaten Bekasi', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (89, 10, 'CBI', 'Kabupaten Bogor', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (90, 10, 'CMS', 'Kabupaten Ciamis', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (91, 10, 'CJR', 'Kabupaten Cianjur', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (92, 10, 'SBR', 'Kabupaten Cirebon', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (93, 10, 'GRT', 'Kabupaten Garut', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (94, 10, 'IDM', 'Kabupaten Indramayu', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (95, 10, 'KWG', 'Kabupaten Karawang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (96, 10, 'KNG', 'Kabupaten Kuningan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (97, 10, 'MJL', 'Kabupaten Majalengka', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (98, 10, 'PAG', 'Kabupaten Pangandaran', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (99, 10, 'PWK', 'Kabupaten Purwakarta', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (100, 10, 'SNG', 'Kabupaten Subang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (101, 10, 'SBM', 'Kabupaten Sukabumi', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (102, 10, 'SMD', 'Kabupaten Sumedang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (103, 10, 'SPA', 'Kabupaten Tasikmalaya', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (104, 10, 'BDG', 'Kota Bandung', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (105, 10, 'BJR', 'Kota Banjar', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (106, 10, 'BKS', 'Kota Bekasi', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (107, 10, 'BGR', 'Kota Bogor', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (108, 10, 'CMH', 'Kota Cimahi', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (109, 10, 'CBN', 'Kota Cirebon', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (110, 10, 'DPK', 'Kota Depok', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (111, 10, 'SKB', 'Kota Sukabumi', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (112, 10, 'TSM', 'Kota Tasikmalaya', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (113, 11, 'BNR', 'Kabupaten Banjarnegara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (114, 11, 'PWT', 'Kabupaten Banyumas', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (115, 11, 'BTG', 'Kabupaten Batang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (116, 11, 'BLA', 'Kabupaten Blora', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (117, 11, 'BYL', 'Kabupaten Boyolali', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (118, 11, 'BBS', 'Kabupaten Brebes', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (119, 11, 'CLP', 'Kabupaten Cilacap', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (120, 11, 'DMK', 'Kabupaten Demak', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (121, 11, 'PWD', 'Kabupaten Grobogan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (122, 11, 'JPA', 'Kabupaten Jepara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (123, 11, 'KRG', 'Kabupaten Karanganyar', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (124, 11, 'KBM', 'Kabupaten Kebumen', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (125, 11, 'KDL', 'Kabupaten Kendal', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (126, 11, 'KLN', 'Kabupaten Klaten', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (127, 11, 'KDS', 'Kabupaten Kudus', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (128, 11, 'MKD', 'Kabupaten Magelang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (129, 11, 'PTI', 'Kabupaten Pati', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (130, 11, 'KJN', 'Kabupaten Pekalongan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (131, 11, 'PML', 'Kabupaten Pemalang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (132, 11, 'PBG', 'Kabupaten Purbalingga', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (133, 11, 'PWR', 'Kabupaten Purworejo', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (134, 11, 'RBG', 'Kabupaten Rembang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (135, 11, 'UNR', 'Kabupaten Semarang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (136, 11, 'SGN', 'Kabupaten Sragen', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (137, 11, 'SKH', 'Kabupaten Sukoharjo', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (138, 11, 'SLW', 'Kabupaten Tegal', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (139, 11, 'TMG', 'Kabupaten Temanggung', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (140, 11, 'WNG', 'Kabupaten Wonogiri', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (141, 11, 'WSB', 'Kabupaten Wonosobo', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (142, 11, 'MGG', 'Kota Magelang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (143, 11, 'PKL', 'Kota Pekalongan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (144, 11, 'SLT', 'Kota Salatiga', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (145, 11, 'SMG', 'Kota Semarang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (146, 11, 'SKT', 'Kota Surakarta (Solo)', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (147, 11, 'TGL', 'Kota Tegal', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (148, 12, 'BKL', 'Kabupaten Bangkalan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (149, 12, 'BYW', 'Kabupaten Banyuwangi', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (150, 12, 'KNR', 'Kabupaten Blitar', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (151, 12, 'BJN', 'Kabupaten Bojonegoro', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (152, 12, 'BDW', 'Kabupaten Bondowoso', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (153, 12, 'GSK', 'Kabupaten Gresik', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (154, 12, 'JMR', 'Kabupaten Jember', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (155, 12, 'JBG', 'Kabupaten Jombang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (156, 12, 'KDR', 'Kabupaten Kediri', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (157, 12, 'LMG', 'Kabupaten Lamongan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (158, 12, 'LMJ', 'Kabupaten Lumajang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (159, 12, 'MJY', 'Kabupaten Madiun', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (160, 12, 'MGT', 'Kabupaten Magetan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (161, 12, 'KPN', 'Kabupaten Malang', '2019-04-15 18:05:08', '2019-06-30 11:52:08', NULL, 1, 'Super Admin', 'Natalia', NULL, 1);
INSERT INTO `tb_kota` VALUES (162, 12, 'MJK', 'Kabupaten Mojokerto', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (163, 12, 'NJK', 'Kabupaten Nganjuk', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (164, 12, 'NGW', 'Kabupaten Ngawi', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (165, 12, 'PCT', 'Kabupaten Pacitan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (166, 12, 'PMK', 'Kabupaten Pamekasan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (167, 12, 'PSR', 'Kabupaten Pasuruan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (168, 12, 'PNG', 'Kabupaten Ponorogo', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (169, 12, 'KRS', 'Kabupaten Probolinggo', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (170, 12, 'SPG', 'Kabupaten Sampang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (171, 12, 'SDA', 'Kabupaten Sidoarjo', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (172, 12, 'SIT', 'Kabupaten Situbondo', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (173, 12, 'SMP', 'Kabupaten Sumenep', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (174, 12, 'TRK', 'Kabupaten Trenggalek', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (175, 12, 'TBN', 'Kabupaten Tuban', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (176, 12, 'TLG', 'Kabupaten Tulungagung', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (177, 12, 'BTU', 'Kota Batu', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (178, 12, 'BLT', 'Kota Blitar', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (179, 12, 'KDR', 'Kota Kediri', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (180, 12, 'MAD', 'Kota Madiun', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (181, 12, 'MLG', 'Kota Malang', '2019-04-15 18:05:08', '2019-06-17 11:56:35', NULL, 1, 'Super Admin', 'Super Admin', NULL, 1);
INSERT INTO `tb_kota` VALUES (182, 12, 'MJK', 'Kota Mojokerto', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (183, 12, 'PSN', 'Kota Pasuruan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (184, 12, 'PBL', 'Kota Probolinggo', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (185, 12, 'SBY', 'Kota Surabaya', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_kota` VALUES (186, 13, 'BEK', 'Kabupaten Bengkayang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (187, 13, 'PTS', 'Kabupaten Kapuas Hulu', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (188, 13, 'SKD', 'Kabupaten Kayong Utara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (189, 13, 'KTP', 'Kabupaten Ketapang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (190, 13, 'SRY', 'Kabupaten Kubu Raya', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (191, 13, 'NBA', 'Kabupaten Landak', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (192, 13, 'NGP', 'Kabupaten Melawi', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (193, 13, 'MPW', 'Kabupaten Mempawah', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (194, 13, 'SBS', 'Kabupaten Sambas', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (195, 13, 'SAG', 'Kabupaten Sanggau', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (196, 13, 'SED', 'Kabupaten Sekadau', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (197, 13, 'STG', 'Kabupaten Sintang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (198, 13, 'PTK', 'Kota Pontianak', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (199, 13, 'SKW', 'Kota Singkawang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (200, 14, 'PRN', 'Kabupaten Balangan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (201, 14, 'MTP', 'Kabupaten Banjar', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (202, 14, 'MRH', 'Kabupaten Barito Kuala', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (203, 14, 'KGN', 'Kabupaten Hulu Sungai Selatan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (204, 14, 'BRB', 'Kabupaten Hulu Sungai Tengah', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (205, 14, 'AMT', 'Kabupaten Hulu Sungai Utara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (206, 14, 'KBR', 'Kabupaten Kotabaru', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (207, 14, 'TJG', 'Kabupaten Tabalong', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (208, 14, 'BLN', 'Kabupaten Tanah Bumbu', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (209, 14, 'PLI', 'Kabupaten Tanah Laut', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (210, 14, 'RTA', 'Kabupaten Tapin', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (211, 14, 'BJB', 'Kota Banjarbaru', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (212, 14, 'BJM', 'Kota Banjarmasin', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (213, 15, 'BNT', 'Kabupaten Barito Selatan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (214, 15, 'TML', 'Kabupaten Barito Timur', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (215, 15, 'MTW', 'Kabupaten Barito Utara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (216, 15, 'KKN', 'Kabupaten Gunung Mas', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (217, 15, 'KLK', 'Kabupaten Kapuas', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (218, 15, 'KSN', 'Kabupaten Katingan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (219, 15, 'PBU', 'Kabupaten Kotawaringin Barat', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (220, 15, 'SPT', 'Kabupaten Kotawaringin Timur', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (221, 15, 'NGB', 'Kabupaten Lamandau', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (222, 15, 'PRC', 'Kabupaten Murung Raya', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (223, 15, 'PPS', 'Kabupaten Pulang Pisau', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (224, 15, 'KLP', 'Kabupaten Seruyan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (225, 15, 'SKR', 'Kabupaten Sukamara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (226, 15, 'PLK', 'Kota Palangka Raya', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (227, 16, 'TNR', 'Kabupaten Berau', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (228, 16, 'SDW', 'Kabupaten Kutai Barat', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (229, 16, 'TRG', 'Kabupaten Kutai Kartanegara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (230, 16, 'SGT', 'Kabupaten Kutai Timur', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (231, 16, 'UJB', 'Kabupaten Mahakam Ulu', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (232, 16, 'TGT', 'Kabupaten Paser', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (233, 16, 'PNJ', 'Kabupaten Penajam Paser Utara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (234, 16, 'BPP', 'Kota Balikpapan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (235, 16, 'BON', 'Kota Bontang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (236, 16, 'SMR', 'Kota Samarinda', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (237, 17, 'TJS', 'Kabupaten Bulungan (Bulongan)', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (238, 17, 'MLN', 'Kabupaten Malinau', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (239, 17, 'NNK', 'Kabupaten Nunukan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (240, 17, 'TDP', 'Kabupaten Tana Tidung', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (241, 17, 'TAR', 'Kota Tarakan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (242, 18, 'BSB', 'Kabupaten Bintan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (243, 18, 'TBK', 'Kabupaten Karimun', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (244, 18, 'TRP', 'Kabupaten Kepulauan Anambas', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (245, 18, 'DKL', 'Kabupaten Lingga', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (246, 18, 'RAN', 'Kabupaten Natuna', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (247, 18, 'BTM', 'Kota Batam', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (248, 18, 'TPG', 'Kota Tanjung Pinang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (249, 19, 'LIW', 'Kabupaten Lampung Barat', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (250, 19, 'KLA', 'Kabupaten Lampung Selatan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (251, 19, 'GNS', 'Kabupaten Lampung Tengah', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (252, 19, 'SDN', 'Kabupaten Lampung Timur', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (253, 19, 'KTB', 'Kabupaten Lampung Utara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (254, 19, 'MSJ', 'Kabupaten Mesuji', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (255, 19, 'GDT', 'Kabupaten Pesawaran', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (256, 19, 'KRU', 'Kabupaten Pesisir Barat', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (257, 19, 'PRW', 'Kabupaten Pringsewu', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (258, 19, 'KOT', 'Kabupaten Tanggamus', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (259, 19, 'MGL', 'Kabupaten Tulang Bawang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (260, 19, 'TWG', 'Kabupaten Tulang Bawang Barat', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (261, 19, 'BBU', 'Kabupaten Way Kanan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (262, 19, 'BDL', 'Kota Bandar Lampung', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (263, 19, 'MET', 'Kota Metro', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (264, 20, 'NLA', 'Kabupaten Buru', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (265, 20, 'NMR', 'Kabupaten Buru Selatan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (266, 20, 'DOB', 'Kabupaten Kepulauan Aru', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (267, 20, 'TKR', 'Kabupaten Maluku Barat Daya', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (268, 20, 'MSH', 'Kabupaten Maluku Tengah', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (269, 20, 'TUL', 'Kabupaten Maluku Tenggara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (270, 20, 'SML', 'Kabupaten Maluku Tenggara Barat', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (271, 20, 'DRH', 'Kabupaten Seram Bagian Barat', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (272, 20, 'DTH', 'Kabupaten Seram Bagian Timur', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (273, 20, 'AMB', 'Kota Ambon', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (274, 20, 'TUL', 'Kota Tual', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (275, 21, 'JLL', 'Kabupaten Halmahera Barat', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (276, 21, 'LBA', 'Kabupaten Halmahera Selatan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (277, 21, 'WED', 'Kabupaten Halmahera Tengah', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (278, 21, 'MAB', 'Kabupaten Halmahera Timur', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (279, 21, 'TOB', 'Kabupaten Halmahera Utara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (280, 21, 'SNN', 'Kabupaten Kepulauan Sula', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (281, 21, 'DRB', 'Kabupaten Pulau Morotai', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (282, 21, 'BOB', 'Kabupaten Pulau Taliabu', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (283, 21, 'TTE', 'Kota Ternate', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (284, 21, 'TDR', 'Kota Tidore Kepulauan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (285, 22, 'WHO', 'Kabupaten Bima', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (286, 22, 'DPU', 'Kabupaten Dompu', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (287, 22, 'GRG', 'Kabupaten Lombok Barat', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (288, 22, 'PYA', 'Kabupaten Lombok Tengah', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (289, 22, 'SEL', 'Kabupaten Lombok Timur', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (290, 22, 'TJN', 'Kabupaten Lombok Utara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (291, 22, 'SBW', 'Kabupaten Sumbawa', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (292, 22, 'TLW', 'Kabupaten Sumbawa Barat', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (293, 22, 'BIM', 'Kota Bima', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (294, 22, 'MTR', 'Kota Mataram', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (295, 23, 'KLB', 'Kabupaten Alor', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (296, 23, 'ATB', 'Kabupaten Belu', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (297, 23, 'END', 'Kabupaten Ende', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (298, 23, 'LRT', 'Kabupaten Flores Timur', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (299, 23, 'KPG', 'Kabupaten Kupang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (300, 23, 'LWB', 'Kabupaten Lembata', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (301, 23, 'BTN', 'Kabupaten Malaka', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (302, 23, 'RTG', 'Kabupaten Manggarai', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (303, 23, 'LBJ', 'Kabupaten Manggarai Barat', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (304, 23, 'BRG', 'Kabupaten Manggarai Timur', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (305, 23, 'MBY', 'Kabupaten Nagekeo', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (306, 23, 'BJW', 'Kabupaten Ngada', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (307, 23, 'BAA', 'Kabupaten Rote Ndao', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (308, 23, 'SBB', 'Kabupaten Sabu Raijua', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (309, 23, 'MME', 'Kabupaten Sikka', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (310, 23, 'WKB', 'Kabupaten Sumba Barat', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (311, 23, 'TAM', 'Kabupaten Sumba Barat Daya', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (312, 23, 'WBL', 'Kabupaten Sumba Tengah', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (313, 23, 'WGP', 'Kabupaten Sumba Timur', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (314, 23, 'SOE', 'Kabupaten Timor Tengah Selatan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (315, 23, 'KFM', 'Kabupaten Timor Tengah Utara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (316, 23, 'KPG', 'Kota Kupang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (317, 24, 'AGT', 'Kabupaten Asmat', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (318, 24, 'BIK', 'Kabupaten Biak Numfor', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (319, 24, 'TMR', 'Kabupaten Boven Digoel', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (320, 24, 'TIG', 'Kabupaten Deiyai (Deliyai)', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (321, 24, 'KGM', 'Kabupaten Dogiyai', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (322, 24, 'SGP', 'Kabupaten Intan Jaya', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (323, 24, 'JAP', 'Kabupaten Jayapura', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (324, 24, 'WAM', 'Kabupaten Jayawijaya', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (325, 24, 'WRS', 'Kabupaten Keerom', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (326, 24, 'SRU', 'Kabupaten Kepulauan Yapen (Yapen Waropen)', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (327, 24, 'TOM', 'Kabupaten Lanny Jaya', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (328, 24, 'BRM', 'Kabupaten Mamberamo Raya', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (329, 24, 'KBK', 'Kabupaten Mamberamo Tengah', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (330, 24, 'KEP', 'Kabupaten Mappi', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (331, 24, 'MRK', 'Kabupaten Merauke', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (332, 24, 'TIM', 'Kabupaten Mimika', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (333, 24, 'NAB', 'Kabupaten Nabire', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (334, 24, 'KYM', 'Kabupaten Nduga', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (335, 24, 'ERT', 'Kabupaten Paniai', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (336, 24, 'OSB', 'Kabupaten Pegunungan Bintang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (337, 24, 'ILG', 'Kabupaten Puncak', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (338, 24, 'MUL', 'Kabupaten Puncak Jaya', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (339, 24, 'SMI', 'Kabupaten Sarmi', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (340, 24, 'SRW', 'Kabupaten Supiori', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (341, 24, 'KBG', 'Kabupaten Tolikara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (342, 24, 'BTW', 'Kabupaten Waropen', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (343, 24, 'SMH', 'Kabupaten Yahukimo', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (344, 24, 'ELL', 'Kabupaten Yalimo', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (345, 24, 'JAP', 'Kota Jayapura', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (346, 25, 'FFK', 'Kabupaten Fakfak', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (347, 25, 'KMN', 'Kabupaten Kaimana', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (348, 25, 'MNK', 'Kabupaten Manokwari', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (349, 25, 'RNK', 'Kabupaten Manokwari Selatan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (350, 25, 'AFT', 'Kabupaten Maybrat', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (351, 25, 'ANG', 'Kabupaten Pegunungan Arfak', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (352, 25, 'WAS', 'Kabupaten Raja Ampat', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (353, 25, 'AMS', 'Kabupaten Sorong', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (354, 25, 'TMB', 'Kabupaten Sorong Selatan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (355, 25, 'FEF', 'Kabupaten Tambrauw', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (356, 25, 'BTI', 'Kabupaten Teluk Bintuni', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (357, 25, 'RAS', 'Kabupaten Teluk Wondama', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (358, 25, 'SON', 'Kota Sorong', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (359, 26, 'BLS', 'Kabupaten Bengkalis', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (360, 26, 'TBH', 'Kabupaten Indragiri Hilir', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (361, 26, 'RGT', 'Kabupaten Indragiri Hulu', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (362, 26, 'BKN', 'Kabupaten Kampar', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (363, 26, 'TTG', 'Kabupaten Kepulauan Meranti', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (364, 26, 'TLK', 'Kabupaten Kuantan Singingi', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (365, 26, 'PKK', 'Kabupaten Pelalawan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (366, 26, 'UJT', 'Kabupaten Rokan Hilir', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (367, 26, 'PRP', 'Kabupaten Rokan Hulu', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (368, 26, 'SAK', 'Kabupaten Siak', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (369, 26, 'DUM', 'Kota Dumai', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (370, 26, 'PBR', 'Kota Pekanbaru', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (371, 27, 'MJN', 'Kabupaten Majene', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (372, 27, 'MMS', 'Kabupaten Mamasa', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (373, 27, 'MAM', 'Kabupaten Mamuju', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (374, 27, 'TBD', 'Kabupaten Mamuju Tengah', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (375, 27, 'PKY', 'Kabupaten Mamuju Utara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (376, 27, 'PLW', 'Kabupaten Polewali Mandar', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (377, 28, 'BAN', 'Kabupaten Bantaeng', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (378, 28, 'BAR', 'Kabupaten Barru', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (379, 28, 'WTP', 'Kabupaten Bone', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (380, 28, 'BLK', 'Kabupaten Bulukumba', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (381, 28, 'ENR', 'Kabupaten Enrekang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (382, 28, 'SGM', 'Kabupaten Gowa', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (383, 28, 'JNP', 'Kabupaten Jeneponto', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (384, 28, 'BEN', 'Kabupaten Selayar (Kepulauan Selayar)', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (385, 28, 'PLP', 'Kabupaten Luwu', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (386, 28, 'MLL', 'Kabupaten Luwu Timur', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (387, 28, 'MSB', 'Kabupaten Luwu Utara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (388, 28, 'MRS', 'Kabupaten Maros', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (389, 28, 'PKJ', 'Kabupaten Pangkajene Kepulauan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (390, 28, 'PIN', 'Kabupaten Pinrang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (391, 28, 'SDR', 'Kabupaten Sidenreng Rappang (Sidrap)', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (392, 28, 'SNJ', 'Kabupaten Sinjai', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (393, 28, 'WNS', 'Kabupaten Soppeng', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (394, 28, 'TKA', 'Kabupaten Takalar', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (395, 28, 'MAK', 'Kabupaten Tana Toraja', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (396, 28, 'RTP', 'Kabupaten Toraja Utara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (397, 28, 'SKG', 'Kabupaten Wajo', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (398, 28, 'MKS', 'Kota Makassar', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (399, 28, 'PLP', 'Kota Palopo', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (400, 28, 'PRE', 'Kota Parepare', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (401, 29, 'LWK', 'Kabupaten Banggai', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (402, 29, 'SKN', 'Kabupaten Banggai Kepulauan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (403, 29, 'BGI', 'Kabupaten Banggai Laut', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (404, 29, 'BUL', 'Kabupaten Buol', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (405, 29, 'DGL', 'Kabupaten Donggala', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (406, 29, 'BGK', 'Kabupaten Morowali', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (407, 29, 'KND', 'Kabupaten Morowali Utara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (408, 29, 'PRG', 'Kabupaten Parigi Moutong', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (409, 29, 'PSO', 'Kabupaten Poso', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (410, 29, 'SGB', 'Kabupaten Sigi', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (411, 29, 'APN', 'Kabupaten Tojo Una-Una', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (412, 29, 'TLI', 'Kabupaten Toli-Toli', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (413, 29, 'PAL', 'Kota Palu', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (414, 30, 'RMB', 'Kabupaten Bombana', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (415, 30, 'PSW', 'Kabupaten Buton', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (416, 30, 'BAG', 'Kabupaten Buton Selatan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (417, 30, 'LBK', 'Kabupaten Buton Tengah', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (418, 30, 'BNG', 'Kabupaten Buton Utara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (419, 30, 'KKA', 'Kabupaten Kolaka', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (420, 30, 'TRW', 'Kabupaten Kolaka Timur', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (421, 30, 'LSS', 'Kabupaten Kolaka Utara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (422, 30, 'UNH', 'Kabupaten Konawe', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (423, 30, 'LGR', 'Kabupaten Konawe Kepulauan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (424, 30, 'ADL', 'Kabupaten Konawe Selatan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (425, 30, 'WGD', 'Kabupaten Konawe Utara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (426, 30, 'RAH', 'Kabupaten Muna', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (427, 30, 'SWG', 'Kabupaten Muna Barat', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (428, 30, 'WGW', 'Kabupaten Wakatobi', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (429, 30, 'BAU', 'Kota Baubau', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (430, 30, 'KDI', 'Kota Kendari', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (431, 31, 'LLK', 'Kabupaten Bolaang Mongondow', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (432, 31, 'BLU', 'Kabupaten Bolaang Mongondow Selatan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (433, 31, 'TTY', 'Kabupaten Bolaang Mongondow Timur', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (434, 31, 'BRK', 'Kabupaten Bolaang Mongondow Utara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (435, 31, 'THN', 'Kabupaten Kepulauan Sangihe', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (436, 31, 'ODS', 'Kabupaten Kepulauan Siau Tagulandang Biaro (Sitaro)', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (437, 31, 'MGN', 'Kabupaten Kepulauan Talaud', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (438, 31, 'TNN', 'Kabupaten Minahasa', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (439, 31, 'AMR', 'Kabupaten Minahasa Selatan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (440, 31, 'RTN', 'Kabupaten Minahasa Tenggara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (441, 31, 'ARM', 'Kabupaten Minahasa Utara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (442, 31, 'BIT', 'Kota Bitung', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (443, 31, 'KTG', 'Kota Kotamobagu', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (444, 31, 'MND', 'Kota Manado', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (445, 31, 'TMH', 'Kota Tomohon', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (446, 32, 'LBB', 'Kabupaten Agam', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (447, 32, 'PLJ', 'Kabupaten Dharmasraya', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (448, 32, 'TPT', 'Kabupaten Kepulauan Mentawai', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (449, 32, 'SRK', 'Kabupaten Lima Puluh Kota', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (450, 32, 'NPM', 'Kabupaten Padang Pariaman', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (451, 32, 'LBS', 'Kabupaten Pasaman', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (452, 32, 'SPE', 'Kabupaten Pasaman Barat', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (453, 32, 'PNN', 'Kabupaten Pesisir Selatan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (454, 32, 'MRJ', 'Kabupaten Sijunjung (Sawah Lunto Sijunjung)', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (455, 32, 'ARS', 'Kabupaten Solok', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (456, 32, 'PDA', 'Kabupaten Solok Selatan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (457, 32, 'BSK', 'Kabupaten Tanah Datar', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (458, 32, 'BKT', 'Kota Bukittinggi', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (459, 32, 'PAD', 'Kota Padang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (460, 32, 'PDP', 'Kota Padang Panjang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (461, 32, 'PMN', 'Kota Pariaman', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (462, 32, 'PYH', 'Kota Payakumbuh', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (463, 32, 'SWL', 'Kota Sawahlunto', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (464, 32, 'SLK', 'Kota Solok', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (465, 33, 'PKB', 'Kabupaten Banyuasin', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (466, 33, 'TBG', 'Kabupaten Empat Lawang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (467, 33, 'LHT', 'Kabupaten Lahat', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (468, 33, 'MRE', 'Kabupaten Muara Enim', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (469, 33, 'SKY', 'Kabupaten Musi Banyuasin', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (470, 33, 'MBL', 'Kabupaten Musi Rawas', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (471, 33, 'RPT', 'Kabupaten Musi Rawas Utara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (472, 33, 'IDL', 'Kabupaten Ogan Ilir', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (473, 33, 'KAG', 'Kabupaten Ogan Komering Ilir', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (474, 33, 'BTA', 'Kabupaten Ogan Komering Ulu', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (475, 33, 'MRD', 'Kabupaten Ogan Komering Ulu Selatan (Oku Selatan)', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (476, 33, 'MPR', 'Kabupaten Ogan Komering Ulu Timur (Oku Timur)', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (477, 33, 'TLB', 'Kabupaten Penukal Abab Lematang Ilir', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (478, 33, 'LLG', 'Kota Lubuk Linggau', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (479, 33, 'PGA', 'Kota Pagar Alam', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (480, 33, 'PLG', 'Kota Palembang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (481, 33, 'PBM', 'Kota Prabumulih', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (482, 34, 'KIS', 'Kabupaten Asahan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (483, 34, 'LMP', 'Kabupaten Batu Bara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (484, 34, 'SDK', 'Kabupaten Dairi', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (485, 34, 'LBP', 'Kabupaten Deli Serdang', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (486, 34, 'DLS', 'Kabupaten Humbang Hasundutan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (487, 34, 'KBJ', 'Kabupaten Karo', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (488, 34, 'RAP', 'Kabupaten Labuhanbatu', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (489, 34, 'KPI', 'Kabupaten Labuhanbatu Selatan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (490, 34, 'AKK', 'Kabupaten Labuhanbatu Utara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (491, 34, 'STB', 'Kabupaten Langkat', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (492, 34, 'PYB', 'Kabupaten Mandailing Natal', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (493, 34, 'GST', 'Kabupaten Nias', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (494, 34, 'LHM', 'Kabupaten Nias Barat', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (495, 34, 'TLD', 'Kabupaten Nias Selatan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (496, 34, 'LTU', 'Kabupaten Nias Utara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (497, 34, 'SBH', 'Kabupaten Padang Lawas', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (498, 34, 'GNT', 'Kabupaten Padang Lawas Utara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (499, 34, 'SAL', 'Kabupaten Pakpak Bharat', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (500, 34, 'PRR', 'Kabupaten Samosir', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (501, 34, 'SRH', 'Kabupaten Serdang Bedagai', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (502, 34, 'PMS', 'Kabupaten Simalungun', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (503, 34, 'PSP', 'Kabupaten Tapanuli Selatan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (504, 34, 'SBG', 'Kabupaten Tapanuli Tengah', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (505, 34, 'TRT', 'Kabupaten Tapanuli Utara', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (506, 34, 'BLG', 'Kabupaten Toba Samosir', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (507, 34, 'BNJ', 'Kota Binjai', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (508, 34, 'GST', 'Kota Gunungsitoli', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (509, 34, 'MDN', 'Kota Medan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (510, 34, 'PSP', 'Kota Padang Sidempuan', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (511, 34, 'PMS', 'Kota Pematangsiantar', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (512, 34, 'SBG', 'Kota Sibolga', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (513, 34, 'TJB', 'Kota Tanjung Balai', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);
INSERT INTO `tb_kota` VALUES (514, 34, 'TBT', 'Kota Tebing Tinggi', '2019-04-15 18:05:08', NULL, NULL, 1, 'Super Admin', NULL, NULL, 0);

-- ----------------------------
-- Table structure for tb_ppl
-- ----------------------------
DROP TABLE IF EXISTS `tb_ppl`;
CREATE TABLE `tb_ppl`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topik` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `penyelenggara` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tanggal` date NULL DEFAULT NULL,
  `tempat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `narasumber` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `jumlah_peserta` int(11) NULL DEFAULT NULL,
  `skppl` int(11) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_ppl
-- ----------------------------
INSERT INTO `tb_ppl` VALUES (1, 'y87tioh', 'asdasdsa', '2015-03-26', 'sadsadsa', 'dasdsada', 100, 8, '2021-04-15 09:39:20', NULL, NULL);

-- ----------------------------
-- Table structure for tb_provinsi
-- ----------------------------
DROP TABLE IF EXISTS `tb_provinsi`;
CREATE TABLE `tb_provinsi`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `kode` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_ro` int(11) NULL DEFAULT NULL,
  `keterangan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `created_user_id` int(3) NULL DEFAULT NULL,
  `created_user` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_user` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deleted_user` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `is_visible` int(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 35 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_provinsi
-- ----------------------------
INSERT INTO `tb_provinsi` VALUES (1, 'ID-AC', 21, 'Aceh', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (2, 'ID-BA', 1, 'Bali', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (3, 'ID-BB', 2, 'Kepulauan Bangka Belitung', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (4, 'ID-BT', 3, 'Banten', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (5, 'ID-BE', 4, 'Bengkulu', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (6, 'ID-YO', 5, 'Daerah Istimewa Yogyakarta', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (7, 'ID-JB', 6, 'Daerah Khusus Ibukota Jakarta', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (8, 'ID-GO', 7, 'Gorontalo', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (9, 'ID-JA', 8, 'Jambi', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (10, 'ID-JB', 9, 'Jawa Barat', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (11, 'ID-JT', 10, 'Jawa Tengah', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (12, 'ID-JI', 11, 'Jawa Timur', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (13, 'ID-KB', 12, 'Kalimantan Barat', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (14, 'ID-KS', 13, 'Kalimantan Selatan', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (15, 'ID-KT', 14, 'Kalimantan Tengah', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (16, 'ID-Kl', 15, 'Kalimantan Timur', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (17, 'ID-KU', 16, 'Kalimantan Utara', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (18, 'ID-KR', 17, 'Kepulauan Riau', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (19, 'ID-LA', 18, 'Lampung', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (20, 'ID-MA', 19, 'Maluku', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (21, 'ID-MU', 20, 'Maluku Utara', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (22, 'ID-NB', 22, 'Nusa Tenggara Barat', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (23, 'ID-NT', 23, 'Nusa Tenggara Timur', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (24, 'ID-PA', 24, 'Papua', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (25, 'ID-PB', 25, 'Papua Barat ', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (26, 'ID-RI', 26, 'Riau', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (27, 'ID-SR', 27, 'Sulawesi Barat', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (28, 'ID-SN', 28, 'Sulawesi Selatan', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (29, 'ID-ST', 29, 'Sulawesi Tengah', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (30, 'ID-SG', 30, 'Sulawesi Tenggara', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (31, 'ID-SA', 31, 'Sulawesi Utara', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (32, 'ID-SB', 32, 'Sumatra Barat', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (33, 'ID-SS', 33, 'Sumatra Selatan', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);
INSERT INTO `tb_provinsi` VALUES (34, 'ID-SU', 34, 'Sumatra Utara', '2019-04-15 18:02:49', NULL, NULL, 1, 'Super Admin', NULL, NULL, 1);

SET FOREIGN_KEY_CHECKS = 1;
